package com.company;

public class RevString {

    public static void main(String[] args) {
        String textToReverseOdd = "exampleTest";
        RevString.reverseString(textToReverseOdd);
    }

    public static String reverseString(String textToReverse){
        char[] charArray = textToReverse.toCharArray();
        for (int i = 0; i < charArray.length/2 ; i++)
        {
            char leftChar = charArray[i];
            charArray[i] = charArray[charArray.length - 1 - i];
            charArray[charArray.length - i -1] = leftChar;
        }
        return new String(charArray);
    }

    public static String reverseStringUsingStringBuffer(String textToReverse)
    {
        return new StringBuffer(textToReverse).reverse().toString();
    }
}

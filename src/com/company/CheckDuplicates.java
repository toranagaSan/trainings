package com.company;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

/**
 * @author Pawel Tarsa (SG0953429)
 * @since 3/3/2017.
 */
public class CheckDuplicates {

    public static void main(String[] args){
        List<Integer> dataToCheck = Arrays.asList(new Integer[]{1, 1, 2, 3, 4, 4, 5, 1, 7});
        List<Integer> potentialDuplicates = getDuplicates(dataToCheck);
        assertTrue(potentialDuplicates.contains(1));
        assertTrue(potentialDuplicates.contains(4));
    }

    public static List<Integer> getDuplicates(List<Integer> listToCheck){
        HashMap<Integer, Integer> register = new HashMap<>();
        for (Integer record: listToCheck){
            if (register.get(record) == null){
                register.put(record, 1);
            } else {
                register.put(record, register.get(record) + 1 ) ;
            }
        }
        List<Integer> duplicates = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry: register.entrySet()){
            if (entry.getValue() > 1)
                duplicates.add(entry.getKey());
        }

        duplicates = register.entrySet().stream()
                .filter(e -> register.get(e.getKey()) > 1)
                .map(f -> f.getKey())
                .collect(Collectors.toList());

        return duplicates;
    }
}

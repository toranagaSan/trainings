package com.company;

import com.company.person.Person;
import com.company.person.PersonFactory;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

/**
 * @author Pawel Tarsa (SG0953429)
 * @since 3/6/2017.
 */
public class StreamsTraining {

    public static void main(String[] args) {
        StreamsTraining st = new StreamsTraining();
        List<Person> listOfPersons = st.getExampleListOfPersons();
        List<?> filteredList;
        filteredList = listOfPersons.stream()
                .filter(p -> p.getAge() > 10)
                .collect(Collectors.toList());
        assertTrue(filteredList.size() == 4);
        filteredList = listOfPersons.stream().sorted(Comparator.comparing(Person::getAge).reversed()).collect(Collectors.toList());
        assertTrue(((Person) filteredList.get(0)).getAge() == 21);
        assertTrue(((Person) filteredList.get(4)).getAge() == 10);
        filteredList = listOfPersons.stream().map(p -> p.getAge()).collect(Collectors.toList());
        assertTrue(filteredList.get(0).getClass().equals(Integer.class));

        filteredList = listOfPersons.stream()
                .filter(p -> p.getAge() > 10)
                .limit(3)
                .collect(Collectors.toList());
        assertTrue(filteredList.size() == 3);

        Optional<Person> optionalPerson = listOfPersons.stream().filter(p -> p.getFirstName().equals("x")).findAny();
        assertTrue(optionalPerson.isPresent());

        Person optionalPersonAlwaysAvailable = listOfPersons.stream().filter(p -> p.getFirstName().equals("xxx"))
                .findAny().orElse(PersonFactory.getPerson(Person.class));
        assertTrue(optionalPersonAlwaysAvailable.getAge() == 0 &&
                optionalPersonAlwaysAvailable.getFirstName().equals("test"));
    }

    public List<Person> getExampleListOfPersons(){
        return Arrays.asList(new Person[] {
           new Person("a", "b", 10),
           new Person("a", "c", 15),
           new Person("x", "x", 20),
           new Person("z", "z", 20),
           new Person("w", "w", 21),
        });
    }

}

package com.company;

import java.util.Arrays;

/**
 * @author Pawel Tarsa (SG0953429)
 * @since 3/4/2017.
 */
public class CheckIfAnagram {

    public static void main(String[] args) {

    }

    public static boolean isAnagram(String firstWord, String secondWord) {
        int lengthOfString = firstWord.length();
        if (secondWord.length() != lengthOfString)
            return false;
        if (secondWord.toUpperCase().equals(firstWord.toUpperCase()))
            return false;
        char[] charsOfFirstWord = firstWord.toCharArray();
        for (int i = 0 ; i < lengthOfString ; i++)
        {
            if (secondWord.indexOf(charsOfFirstWord[i]) == -1)
                return false;
        }
        return true;
    }

    public static boolean checkAnagram(String firstWord, String secondWord) {
        char[] firstWordChars = firstWord.toLowerCase().toCharArray();
        char[] secondWordChars = secondWord.toLowerCase().toCharArray();
        Arrays.sort(firstWordChars);
        Arrays.sort(secondWordChars);
        return Arrays.equals(firstWordChars, secondWordChars);
    }

    public static boolean iAnagram(String firstWord, String secondWord) {
        return true;
    }
}

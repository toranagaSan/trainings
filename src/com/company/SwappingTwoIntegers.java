package com.company;

import static org.junit.Assert.assertTrue;

/**
 * @author Pawel Tarsa (SG0953429)
 * @since 3/4/2017.
 */
public class SwappingTwoIntegers {

    public static void main(String[] args) {
        Integer a = 2;
        Integer b = 3;

        assertTrue(a.intValue() == 2 && b.intValue() == 3);
        a = a + b;
        b = a - b;
        a = a - b;
        assertTrue(a.intValue() == 3 && b.intValue() == 2);
    }

}

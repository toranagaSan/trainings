package com.company;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @author Pawel Tarsa (SG0953429)
 * @since 3/3/2017.
 */
public class Fibanacci {

    public static void main(String[] args) {
//        calculateFibanacciSeries(5).forEach(e -> System.out.println(e.intValue()));
//        calculateFibanacciSeries(10);
        System.out.println(fibonacci(10));

    }

    public static List<Integer> calculateFibanacciSeries(int n) {
        System.out.println(System.currentTimeMillis());
        List<Integer> fibSeries = new ArrayList<>();
        fibSeries.add(1);
        fibSeries.add(1);
        for (int i = 2; i < n; i++)
            fibSeries.add(fibSeries.get(i - 1) + fibSeries.get(i - 2));
        System.out.println(System.currentTimeMillis());
        return fibSeries;
    }

    public static List<Integer> calculateFibanacciSeriesRecurent(int lengthOfSeries) {
        //todo add AOP to logging
//        long startTime = System.currentTimeMillis();
        System.out.println(fibonacci(10));
        return null;
//        System.out.println(System.currentTimeMillis() - startTime);
    }


    public static int fibonacci(int number) {
        if (number == 1 || number == 2) {
            return 1;
        }
        return fibonacci(number - 1) + fibonacci(number - 2); //tail recursion }

    }

}

package com.company.design_patterns;

import static org.junit.Assert.assertTrue;

/**
 * @author Pawel Tarsa (SG0953429)
 * @since 3/6/2017.
 *
 * There can be EAGER initialization like:
 *      private static final SignletonExample instance = new SignletonExample();
 *  or in static block (better, because make possible to catch exceptions):
 *      static {
 *          instance = new SingletonExample();
 *      }
 *  or lazy like below (with synchronized method to getting instance (lazy)) it becomes thread-safe.
 */
public class SingletonExample {

    private static SingletonExample singleton;

    static {
        singleton = null;
    }

    public static void main(String[] args) {
        SingletonExample se;
        try {
            se = new SingletonExample();
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        se = SingletonExample.getInstance();
        assertTrue(se.equals(SingletonExample.getInstance()));
    }

    private SingletonExample() {
        singleton = this;
    }

    public static SingletonExample getInstance(){
        if (singleton == null)
            singleton = new SingletonExample();
        return singleton;
    }
}

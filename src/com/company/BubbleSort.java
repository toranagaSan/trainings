package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * @author Pawel Tarsa (SG0953429)
 * @since 3/3/2017.
 */
public class BubbleSort {

    public static void main(String... args){
        int[] dataToSort = {32, 39,21, 45, 23, 3};
        bubbleSort(dataToSort);
    }

    public static List<Integer> bubbleSort(int[] dataToSorting){
        for (int i = 0; i < dataToSorting.length - 1; i++){
            for (int j = 1; j < dataToSorting.length - i; j++) {
                if (dataToSorting[j] < dataToSorting[j - 1]) {
                    int tmpVar = dataToSorting[j];
                    dataToSorting[j] = dataToSorting[j - 1];
                    dataToSorting[j - 1] = tmpVar;
                }
            }
        }
        List<Integer> sortedList = new ArrayList<>();
        for (Integer rec: dataToSorting){
            sortedList.add(rec);
        }
        return sortedList;
    }

}

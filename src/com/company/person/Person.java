package com.company.person;

/**
 * @author Pawel Tarsa (SG0953429)
 * @since 3/6/2017.
 */
public class Person {

    private String firstName;
    private String secondName;
    private Integer age;

    public Person() {
        this.firstName = "test";
        this.secondName = "test";
        this.age = 0;
    }

    public Person(String firstName, String secondName, Integer age) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
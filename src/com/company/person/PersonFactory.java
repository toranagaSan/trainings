package com.company.person;

/**
 * @author Pawel Tarsa (SG0953429)
 * @since 3/6/2017.
 */
public class PersonFactory {

    public static void main(String[] args) {

    }

    public static Person getPerson(Class<Person> clazz, Object... args) {
        try{
            // todo hujowizna, wywali sie przy pierwszej okazji (probie przekazania argumentow). Jak to zrobic
            // generycznie??
            if (args.length == 0)
                return clazz.getConstructor().newInstance();
            return clazz.getConstructor().newInstance(args);
        } catch (Exception e){
            return null;
        }
    }
}

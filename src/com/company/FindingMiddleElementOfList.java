package com.company;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * @author Pawel Tarsa (SG0953429)
 * @since 3/4/2017.
 */
public class FindingMiddleElementOfList {

    public static void main(String[] args) {
        LinkedList<Integer> listToCheck = new LinkedList<>();
        listToCheck.add(1);
        listToCheck.add(2);
        listToCheck.add(3);
        listToCheck.add(4);
        listToCheck.add(5);
        assertTrue(findMiddle(listToCheck) == 3);
    }

    public static Integer findMiddle(LinkedList<Integer> linkedList) {
        int currentPosition = 0;
        Iterator<Integer> middleIterator = linkedList.iterator();
        for (Integer iter: linkedList){
            if(currentPosition % 2 == 1){
                middleIterator.next();
            }
            currentPosition++;
        }
        if (currentPosition % 2 == 1)
            return middleIterator.next();

        return middleIterator.next();
    }
}

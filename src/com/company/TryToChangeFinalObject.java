package com.company;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * @author Pawel Tarsa (SG0953429)
 * @since 3/14/2017.
 */
public class TryToChangeFinalObject {

    private final Integer integerInstanceObjectToChange;
    private final static Integer integerStaticObjectToChange;
    private Integer i = new Integer("10");

    static {
        integerStaticObjectToChange = 1;
    }

    public TryToChangeFinalObject(){
        integerInstanceObjectToChange = i;
    }

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        TryToChangeFinalObject t = new TryToChangeFinalObject();

        System.out.println(t.i);
        System.out.println(t.integerInstanceObjectToChange);

        t.i = new Integer("11");
        System.out.println(t.i);
        System.out.println(t.integerInstanceObjectToChange);
//        t.changeValues();
    }

    public void changeValues() throws NoSuchFieldException, IllegalAccessException {
        Field[] fields = TryToChangeFinalObject.class.getFields();
        Field instanceObjectToChange = TryToChangeFinalObject.class.getDeclaredField("integerInstanceObjectToChange");
        instanceObjectToChange.setAccessible(true);
        System.out.println(this.integerInstanceObjectToChange);
        instanceObjectToChange.setInt(this, 5);
        System.out.println(this.integerInstanceObjectToChange);
    }
}

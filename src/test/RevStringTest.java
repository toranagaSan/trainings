package test;

import com.company.RevString;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertTrue;

/**
 * @author Pawel Tarsa (SG0953429)
 * @since 3/1/2017.
 */

@RunWith(Parameterized.class)
public class RevStringTest {

    private String parameter;
    private String result;

    @Parameters(name = "Execution with: {0}, expected result: {1}")
    public static Collection<Object[]> data(){
        return Arrays.asList(new Object[][]{
                {"exampleTest", "tseTelpmaxe"},
                {"exampleTest1", "1tseTelpmaxe"}
        });
    }

    public RevStringTest(String testParameter, String result) {
        this.parameter = testParameter;
        this.result = result;
    }

    @Test
    public void testReverseString(){
        String reversedText = RevString.reverseString(parameter);
        assertTrue(reversedText.equals(result));
    }
}

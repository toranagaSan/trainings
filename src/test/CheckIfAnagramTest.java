package test;

import com.company.CheckIfAnagram;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Pawel Tarsa (SG0953429)
 * @since 3/4/2017.
 */
public class CheckIfAnagramTest {

    @Test
    public void testIsAnagram() {
        assertTrue(CheckIfAnagram.isAnagram("word", "wrdo"));
        assertTrue(CheckIfAnagram.isAnagram("mary", "army"));
        assertTrue(CheckIfAnagram.isAnagram("stop", "tops"));
        assertTrue(CheckIfAnagram.isAnagram("boat", "btoa"));
        assertFalse(CheckIfAnagram.isAnagram("pure", "in"));
        assertFalse(CheckIfAnagram.isAnagram("fill", "fil"));
        assertFalse(CheckIfAnagram.isAnagram("b", "bbb"));
        assertFalse(CheckIfAnagram.isAnagram("ccc", "ccccccc"));
//        assertTrue(CheckIfAnagram.isAnagram("a", "a"));
        assertFalse(CheckIfAnagram.isAnagram("sleep", "slep"));
    }

    @Test
    public void testIAnagram() {
        assertTrue(CheckIfAnagram.iAnagram("word", "wrdo"));
        assertTrue(CheckIfAnagram.iAnagram("boat", "btoa"));
        assertFalse(CheckIfAnagram.iAnagram("pure", "in"));
        assertFalse(CheckIfAnagram.iAnagram("fill", "fil"));
        assertTrue(CheckIfAnagram.iAnagram("a", "a"));
        assertFalse(CheckIfAnagram.iAnagram("b", "bbb"));
        assertFalse(CheckIfAnagram.iAnagram("ccc", "ccccccc"));
        assertFalse(CheckIfAnagram.iAnagram("sleep", "slep"));
    }

    @Test
    public void testcheckAnagram() {
        assertTrue(CheckIfAnagram.checkAnagram("word", "wrdo"));
        assertFalse(CheckIfAnagram.checkAnagram("b", "bbb"));
        assertFalse(CheckIfAnagram.checkAnagram("ccc", "ccccccc"));
        assertTrue(CheckIfAnagram.checkAnagram("a", "a"));
        assertFalse(CheckIfAnagram.checkAnagram("sleep", "slep"));
        assertTrue(CheckIfAnagram.checkAnagram("boat", "btoa"));
        assertFalse(CheckIfAnagram.checkAnagram("pure", "in"));
        assertFalse(CheckIfAnagram.checkAnagram("fill", "fil"));
    }


}

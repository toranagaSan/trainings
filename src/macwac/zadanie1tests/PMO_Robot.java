import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class PMO_Robot implements RobotInterface, PMO_Testable {
	private PMO_AtomicCounter maxParallelUsage = PMO_CountersFactory.prepareCommonMaxStorageCounter();
	private PMO_AtomicCounter parallelUsage = PMO_CountersFactory.prepareCounterWithMaxStorageSet();
	private PMO_AtomicCounter[] USparallelUsageTable;
	private long sleep;
	private CyclicBarrier disposableBarrier;
	private List<PMO_US> avaiableUS;
	private AtomicBoolean robotsCanNotMove;
	private Map<Integer, Integer> unfinishedJobs;
	private boolean accidentNotExpected = true;
	private ParcelInterface parcelToAddAfterAccident;
	private int destinationUS4parcel;
	private BrokerInterface broker;
	private AtomicBoolean accidentFlag = new AtomicBoolean(false);
	private static AtomicInteger helper = new AtomicInteger();
	private static AtomicInteger concurrentUse = new AtomicInteger();
	private static int robotsCounter;
	private final int robotID;
	protected Collection<Integer> toDo;
	private AtomicBoolean chosenOne;
	private AtomicBoolean banRobot;
	private AtomicInteger globalBrokenRobotsCounter;

	public PMO_Robot(PMO_AtomicCounter[] USparallelUsageTable, List<PMO_US> avaiableUS,
			AtomicBoolean robotsCanNotMove) {
		this.USparallelUsageTable = USparallelUsageTable;
		maxParallelUsage.setFailPredicate(PMO_IntPredicateFactory.moreThenOne());
		this.robotsCanNotMove = robotsCanNotMove;
		this.avaiableUS = avaiableUS;
		robotID = robotsCounter++;
	}

	public int getRobotID() {
		return robotID;
	}

	public void setToDo(Collection<Integer> toDo) {
		this.toDo = toDo;
	}

	public void setUnfinishedJobs(Map<Integer, Integer> unfinishedJobs) {
		this.unfinishedJobs = unfinishedJobs;
	}

	public void setBanRobot(AtomicBoolean banRobot) {
		this.banRobot = banRobot;
	}

	public void setParcelToAddAfterAccident(ParcelInterface parcelToAddAfterAccident, int destinationUS4parcel,
			BrokerInterface broker, AtomicBoolean chosenOne, AtomicBoolean banRobot,
			AtomicInteger globalBrokenRobotsCounter) {
		this.parcelToAddAfterAccident = parcelToAddAfterAccident;
		this.destinationUS4parcel = destinationUS4parcel;
		this.broker = broker;
		accidentNotExpected = false; // ustawiamy, ze robot ma sie uszkodzic
		this.chosenOne = chosenOne;
		this.banRobot = banRobot;
		this.globalBrokenRobotsCounter = globalBrokenRobotsCounter;
	}

	public void setSleep(long sleep) {
		this.sleep = sleep;
	}

	public void blockRobot() {
		while (robotsCanNotMove.get()) {
			synchronized (robotsCanNotMove) {
				try {
					if (robotsCanNotMove.get())
						robotsCanNotMove.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void testUnfinishedJobs4Peek(int USid, int parcelID) {
		if (unfinishedJobs != null) {
			if (banRobot != null) {
				boolean ban = false;
				synchronized (unfinishedJobs) {
					if (!unfinishedJobs.containsKey(parcelID)) {
						println("Ban zostanie uruchomiony, w unfinishedJobs nie ma " + parcelID);
						ban = true;
					} else {
						println("Ban nie zostanie uruchomiony, w unfinishedJobs jest " + parcelID);
						println("Aktualna lista nieukonczonych prac " + unfinishedJobsList());
					}
				}
				if (ban) {
					println("Peek - BAN! - start");
					PMO_TimeHelper.sleep(PMO_Consts.ROBOT_BAN);
					println("Peek - BAN! - stop");
				}
			}

			synchronized (unfinishedJobs) {
				println("Test nieukonczonych zadan");
				if (!unfinishedJobs.isEmpty()) {
					println("Sa nieukonczone zadania");

					// sprawdzamy czy kontynuujemy jedna z prac jesli tak -
					// usuwamy ja z rejestru
					// jesli nie - blad krytyczny

					if (unfinishedJobs.containsKey(parcelID)) {
						if (unfinishedJobs.get(parcelID) == USid) {
							println("OK - robot otrzymal zadanie kontynuacji niedokonczonej pracy");
							println("Usuwam paczke " + parcelID + " z unfinishedJobs");
							unfinishedJobs.remove(parcelID);
							println("Aktualna lista nieukonczonych prac " + unfinishedJobsList());
						}
					} else {
						PMO_CommonErrorLog.error("!!!!! BLAD zlecono robotowi " + robotID
								+ " peek inny niz nieukoczone na skutek awarii zadanie !!!!!");
					}
				} else {
					println("Test nieukonczonych zadan - brak zadan do kontynuacji");
				}
			}

		}

	}

	private void testUsageAfterAccident() {
		if (accidentFlag.get()) {
			PMO_CommonErrorLog.error("BLAD: Robot " + robotID + " zostal uzyty, choc zglosil awarie");
		}
	}

	private void println(String txt) {
		if (PMO_Consts.VERBOSE)
			PMO_SystemOutRedirect.println("[" + PMO_TimeHelper.getTimeFromStart() + "] "
					+ Thread.currentThread().getName() + " Robot " + robotID + "> " + txt);
	}

	private String unfinishedJobsList() {
		StringBuffer b = new StringBuffer();
		unfinishedJobs.forEach((k, v) -> {
			b.append("[ " + k + " -> " + v + "]");
		});
		return b.toString();
	}

	@Override
	public ParcelInterface peek(int USid, int parcelID) throws Exception {
		parallelUsage.incAndStoreMax();
		USparallelUsageTable[USid].incAndStoreMax();
		int cu = concurrentUse.incrementAndGet();

		println("Robots in use " + cu);

		testUsageAfterAccident();
		testUnfinishedJobs4Peek(USid, parcelID);

		println("peek( USid " + USid + ", pid " + parcelID + ")");

		println("Robot-peek przed bariera " + helper.incrementAndGet());
		barrier(disposableBarrier);

		if (disposableBarrier != null) {
			disposableBarrier = null;
			println("---- Przekroczono disposableBarrier -----");
		}

		// w ten sposob mamy gwarancje, ze uszkodzeniu ulegnie tylko jeden robot
		// lub grupa
		if (chosenOne != null) {
			synchronized (chosenOne) {
				if (chosenOne.get()) {
					println("Robot " + robotID + " nie ulegnie awarii");
					accidentNotExpected = true; // naprawiamy robota
				} else {
					if (globalBrokenRobotsCounter != null) {
						if (globalBrokenRobotsCounter.get() > 0) {
							println("Robot " + robotID + " ulegnie awarii");
							if (globalBrokenRobotsCounter.decrementAndGet() == 0)
								chosenOne.set(true);
						}
					} else {
						chosenOne.set(true);
					}
				}
			}
		}

		if (accidentNotExpected) {
			println("Before block");
			blockRobot();

		} else {
			if (robotsCanNotMove.get()) {
				println("Robot zaraz ulegnie awarii");
				broker.push(parcelToAddAfterAccident, destinationUS4parcel);
				toDo.add(parcelToAddAfterAccident.getParcelID());
				println("Przekazano paczke " + parcelToAddAfterAccident.getParcelID() + " do przeslania do US");
				USparallelUsageTable[USid].dec();
				parallelUsage.dec();
				println("Dodaje paczke " + parcelID + " do unfinishedJobs ");
				unfinishedJobs.put(parcelID, USid);
				println("Aktualna lista nieukonczonych prac " + unfinishedJobsList());

				Thread th = new Thread(new Runnable() {

					@Override
					public void run() {
						PMO_TimeHelper.sleep(1000); // sekunda na wznowienie
													// ruchu
													// robotow
						robotsCanNotMove.set(false);
						if (banRobot != null) {
							println("Pozwalamy na ruch wszystkich robotow!");
							synchronized (robotsCanNotMove) {
								robotsCanNotMove.notifyAll();
							}
						} else {
							if (chosenOne != null) {
								println("Pozwalamy na ruch jednego robota!");
								synchronized (robotsCanNotMove) {
									robotsCanNotMove.notify();
								}
								PMO_TimeHelper.sleep(1000); // sekunda na
															// wznowienie
								println("Pozwalamy na ruch wszystkich pozostalych robotow!");
								synchronized (robotsCanNotMove) {
									robotsCanNotMove.notifyAll();
								}
							}
						}
					}
				});
				th.setDaemon(true);
				th.start();
				println("Uruchomiono watek, ktory za 1 sekunde odblokuje roboty");
				println("Zglaszam awarie robota, ktory prznosi paczke numer " + parcelID + " do " + USid);
				accidentFlag.set(true);
				throw new Exception("Robot zostal uszkodzony!");
			}
		}
		// PMO_SystemOutRedirect.println( "Robot can move" );

		PMO_TimeHelper.sleep(sleep);
		ParcelInterface pi = avaiableUS.get(USid).get(parcelID);

		println("peek( USid " + USid + ", pid " + parcelID + ") - done");

		concurrentUse.decrementAndGet();
		USparallelUsageTable[USid].dec();
		parallelUsage.dec();
		return pi;
	}

	protected void setDisposableBarrier(CyclicBarrier barrier) {
		this.disposableBarrier = barrier;
	}

	private void barrier(CyclicBarrier cb) {
		try {
			if (cb != null)
				cb.await();
		} catch (InterruptedException | BrokenBarrierException e) {
			PMO_CommonErrorLog.error("W trakcie await doszlo do pojawienia sie wyjatku " + e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public void push(ParcelInterface parcel, int USid) {
		parallelUsage.incAndStoreMax();
		USparallelUsageTable[USid].incAndStoreMax();
		int cu = concurrentUse.incrementAndGet();

		println("Robots in use " + cu);
		println("Robot->push( USid " + USid + ", pid " + parcel.getParcelID() + ")");

		testUsageAfterAccident();

		if (banRobot != null) {
			println("Push - BAN! - start");
			PMO_TimeHelper.sleep(PMO_Consts.ROBOT_BAN);
			println("Push - BAN! - stop");
		}

		if (unfinishedJobs != null)
			synchronized (unfinishedJobs) {
				if (!unfinishedJobs.isEmpty()) {
					// BLAD ! robot nie moze byc wyslany z paczka
					// gdy jest niepobrana paczka
					PMO_CommonErrorLog.error("!!!!! BLAD zlecono robotowi " + robotID
							+ " push, choc nieukonczono pobrania paczki na skutek awarii !!!!!!");
				}
			}

		println("Robot-push przed bariera " + helper.incrementAndGet());
		barrier(disposableBarrier);

		if (disposableBarrier != null) {
			disposableBarrier = null;
			println("---- Przekroczono disposableBarrier -----");
		}

		blockRobot();

		PMO_TimeHelper.sleep(sleep);
		avaiableUS.get(USid).add(parcel);
		println("Robot->push( UDid " + USid + ", pid " + parcel.getParcelID() + ") - do");

		concurrentUse.decrementAndGet();
		USparallelUsageTable[USid].dec();
		parallelUsage.dec();
	}

	@Override
	public boolean test() {
		boolean result = true;

		if (maxParallelUsage.isFail().get()) {
			PMO_CommonErrorLog.error("Robot wykryl, ze wykonywal wiecej niz jedna czynnosc jednoczesnie ");
			PMO_CommonErrorLog.criticalMistake();
			result = false;
		}

		return result;
	}
}

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ThreadLocalRandom;

public class PMO_TestA implements PMO_RunTestTimeout {

	/*
	 * Test sprawdza czy system efektywnie zarzadza robotami. Przy okazji
	 * testowane sa podstawowe krytyczne wymogi projektu.
	 */

	protected static final int ADDITIONAL_ROBOTS_DELTA = 20;
	protected static final int PARCELS_MULT_DELTA = 2;
	protected static final int PARCELS_MULT_MIN = 5;
	protected static final int US_MIN = 25;
	protected static final int US_DELTA = 10;
	protected static final int ROBOT_SLOWDOWN_MIN = 330;
	protected static final int ROBOT_SLOWDOWN_MULT_10 = 5;
	protected static final int ROBOT_SLOWDOWN_JITTER_MAX = ROBOT_SLOWDOWN_MIN / 3;

	protected int parcelsToDeliver;
	protected int parcelsToReceive;
	protected int parcels;
	protected int USs;
	protected int robots;
	protected int slowDown;
	protected long estimatedExecutionTime;
	protected List<ParcelInterface> parcelsCollection;
	protected Collection<Integer> toDo;
	protected Map<Integer, Integer> parcelID2USid = new TreeMap<>();
	private static int versionCounter;

	protected List<PMO_SenderUser> senders;
	protected List<PMO_ReceiverUser> receivers;
	protected List<PMO_RobotDeliverer> deliverers;
	protected List<PMO_US> USlist;
	protected List<PMO_Robot> robotList;
	protected List<PMO_RunnableAndTestable> allRunnubleObjects;
	protected List<PMO_Testable> allTestableObjects;

	protected Random rnd = ThreadLocalRandom.current();
	protected BrokerInterface broker;
	protected PMO_AllTasksExecutionMeasurementTool timeMeasureTool;
	protected PMO_Factory factory;

	// TODO oprogramowac testy !
	private Set<Integer> globalSetOfParcelsDelivered2US = Collections.synchronizedSet(new TreeSet<>());
	private Set<Integer> globalSetOfParcels2Lose = Collections.synchronizedSet(new TreeSet<>());
	private Set<Integer> globalSetOfParcelsInUSorInUser = Collections.synchronizedSet(new TreeSet<>());

	protected void showTestParameters() {
		PMO_SystemOutRedirect.println("#US                 = " + USs);
		PMO_SystemOutRedirect.println("#Parcels to deliver = " + parcelsToDeliver);
		PMO_SystemOutRedirect.println("#Parcels to receive = " + parcelsToReceive);
		PMO_SystemOutRedirect.println("#Parcels total      = " + parcels);
		PMO_SystemOutRedirect.println("#Robots             = " + robots);
		PMO_SystemOutRedirect.println("Robot slow down     = " + slowDown + " msec");
		PMO_SystemOutRedirect.println("     max jitter     = " + ROBOT_SLOWDOWN_JITTER_MAX + " msec");
		PMO_SystemOutRedirect.println("    min timeout     = " + estimatedExecutionTime + " msec");
	}

	protected void setTestParameters() {
		int multD = PARCELS_MULT_MIN + rnd.nextInt(PARCELS_MULT_DELTA);
		int multR = PARCELS_MULT_MIN + rnd.nextInt(PARCELS_MULT_DELTA);

		USs = US_MIN + rnd.nextInt(US_DELTA);

		// robotow jest nie mniej niz USs, wiec wszystkie US mozna
		// obslugiwac rownoczesnie
		robots = rnd.nextInt(ADDITIONAL_ROBOTS_DELTA) + USs;

		int minUSandRobots = USs; 
		parcelsToDeliver = multD * minUSandRobots;
		parcelsToReceive = multR * minUSandRobots;

		parcels = parcelsToDeliver + parcelsToReceive;

		slowDown = ROBOT_SLOWDOWN_MIN + 10 * rnd.nextInt(ROBOT_SLOWDOWN_MULT_10);

		estimatedExecutionTime = (long) ((slowDown + ROBOT_SLOWDOWN_JITTER_MAX / 2) * (float) parcels / minUSandRobots);
	}

	public void println(String txt) {
		if (PMO_Consts.VERBOSE)
			PMO_SystemOutRedirect.println("[" + PMO_TimeHelper.getTimeFromStart() + "] "
					+ Thread.currentThread().getName() + " TEST > " + txt);
	}
	
	protected void createObjects() {
		senders = new ArrayList<>();
		receivers = new ArrayList<>();
		deliverers = new ArrayList<>();
	}

	protected void generateUSAndRobots() {

		factory.createUSandRobots(USs, robots, false);

		USlist = factory.getUSs();
		robotList = factory.getRobots();
	}

	protected void generateParcelsAndRoutes() {

		assert parcelsCollection != null : "parcelsCollection == null";

		ParcelInterface parcel;
		int destinationUSid;
		toDo = Collections.synchronizedSet(new TreeSet<>());
		for (int i = 0; i < parcels; i++) {
			parcel = factory.createNewParcel();
			parcelsCollection.add(parcel);
			destinationUSid = i % USs;
			// zadanie dodane do listy toDo
			toDo.add(parcel.getParcelID());
			// dodanie zadania do mapy tras
			parcelID2USid.put(parcel.getParcelID(), destinationUSid);
		}
		Collections.shuffle(parcelsCollection);
	}

	protected void robotsBeforeParcels() {
		CyclicBarrier cb = new CyclicBarrier(robots + parcelsToDeliver);
		// bariera ma pozwolic na dodanie paczek dopiero po dodaniu robotow

		// u dostawcow bariera po dostarczeniu robota
		deliverers.forEach(d -> d.setAfterRealRun(cb));

		// u nadawcow paczek przed dostarczeniem paczki
		senders.forEach(s -> s.setBeforeRealRun(cb));
	}

	protected void parcelsBeforeRobots() {
		CyclicBarrier cb = new CyclicBarrier(robots + parcelsToDeliver);
		// bariera ma pozwolic na dodanie paczek dopiero po dodaniu robotow

		// u nadawcow paczek bariera po dostarczeniu robota
		deliverers.forEach(d -> d.setBeforeRealRun(cb));

		// u dostawcow robotow przed dostarczeniem paczki
		senders.forEach(s -> s.setAfterRealRun(cb));
	}

	protected void chooseVersion() {
		int v = (versionCounter++)%3;
		if (v == 0) {
			parcelsBeforeRobots();
			PMO_SystemOutRedirect.println("[Paczki dostarczane przed robotami]");
		}
		if (v == 1) {
			robotsBeforeParcels();
			PMO_SystemOutRedirect.println("[Roboty dostarczane przed paczkami]");
		}
	}

	protected void generateUsers() {
		int index = 0;
		int destination;
		ParcelInterface parcel;
		for (int i = 0; i < parcelsToDeliver; i++) {
			parcel = parcelsCollection.get(index++);
			// TODO uzaleznienie pomiaru od liczby odczytow

			senders.add(new PMO_SenderUser(parcel, parcelID2USid.get(parcel.getParcelID()), true));
		}
		// ponizej mozna dodac losowe gubienie paczek
		for (int i = 0; i < parcelsToReceive; i++) {
			parcel = parcelsCollection.get(index++);
			destination = parcelID2USid.get(parcel.getParcelID());
			receivers.add(new PMO_ReceiverUser(parcel.getParcelID(), destination, parcel));
			USlist.get(destination).preAdd(parcel);
		}
		robotList.forEach(r -> deliverers.add(new PMO_RobotDeliverer(r, true)));
	}

	protected void generateAll() {
		allRunnubleObjects = new ArrayList<>();
		allRunnubleObjects.addAll(deliverers);
		allRunnubleObjects.addAll(receivers);
		allRunnubleObjects.addAll(senders);
		Collections.shuffle(allRunnubleObjects);

		allTestableObjects = new ArrayList<>(allRunnubleObjects);
		allTestableObjects.addAll(robotList);
		allTestableObjects.addAll(USlist);
	}

	protected long rndSymetric(long value, long delta) {
		if ( delta == 0 )
			return value;
		return value + rnd.nextInt((int) delta) - delta / 2;
	}

	protected void setRobotSlowDown( long jitter ) {
		robotList.forEach(r -> r.setSleep(rndSymetric(slowDown, jitter )));
	}

	protected void createAndSetBroker() {
		broker = (BrokerInterface) PMO_GeneralPurposeFabric.fabric("Broker", "BrokerInterface");
		broker.setNumberOfStorageUnits(USs);
		senders.forEach(s -> s.setBroker(broker));
		receivers.forEach(r -> r.setBroker(broker));
		deliverers.forEach(d -> d.setBroker(broker));
	}

	protected void setRoutesInUS() {
		USlist.forEach(u -> u.setParcel2USMap(parcelID2USid));
	}

	protected void setGlobalCollections() {
		USlist.forEach(u -> {
			u.setGlobalSetOfParcels2Lose(globalSetOfParcels2Lose);
			u.setGlobalSetOfParcelsDelivered2US(globalSetOfParcelsDelivered2US);
			u.setGlobalSetOfParcelsInUSorInUser(globalSetOfParcelsInUSorInUser);
			u.setToDo(toDo);
		});

		receivers.forEach(r -> r.setGlobalSetOfParcelsInUSorInUser(globalSetOfParcelsInUSorInUser));
	}

	protected void init() {
		PMO_SystemOutRedirect.println("TEST A_A_A_A_A_A");
		showTestParameters();
		createObjects();
		generateParcelsAndRoutes();
		generateUSAndRobots();
		setRoutesInUS();
		generateUsers();
		chooseVersion();
		generateAll();
		setRobotSlowDown(ROBOT_SLOWDOWN_JITTER_MAX);
		setGlobalCollections();
		createAndSetBroker();
	}

	public PMO_TestA() {
		factory = new PMO_Factory();
		parcelsCollection = new ArrayList<>();
		setTestParameters();
	}

	@Override
	public void run() {
		init();

		int workers = robots + parcelsToDeliver + 2;
		CyclicBarrier cb = new CyclicBarrier(workers);

		deliverers.forEach(d -> d.setAtTheEnd(cb));
		senders.forEach(s -> s.setAtTheEnd(cb));

		timeMeasureTool = new PMO_AllTasksExecutionMeasurementTool(toDo, PMO_Consts.TIME_MEASURE_RESOLUTION);

		// watek odblokowania robotow
		factory.startRobotsTriggerThread(cb);
		// watek odblokowania zegara
		timeMeasureTool.start(cb);

		PMO_ThreadsHelper.createAndStartThreads(allRunnubleObjects, true);
	}

	protected boolean executeTests(PMO_Testable... tests) {
		return executeTests(Arrays.asList(tests));
	}

	protected boolean executeTests(List<PMO_Testable> tests) {
		boolean result = true;

		PMO_SystemOutRedirect.println("[executeTests " + tests.size() + "]");
		for (PMO_Testable test : tests) {
			result &= test.test();
		}
		return result;
	}

	@Override
	public boolean test() {

		long systemWorkTime = timeMeasureTool.getResult();
		if (systemWorkTime < 0) {
			PMO_CommonErrorLog.error("BLAD: TestA nie zostal zaliczony. ");
			PMO_CommonErrorLog.error("W przewidywanym czasie system nie byl w stanie obsluzyc zlecenia klientow");
			PMO_CommonErrorLog.error("Do wykonania pozostalo " + toDo.size() + " zadan");
			return false;
		} else {
			if (systemWorkTime > (long) (1.1 * estimatedExecutionTime)) {
				PMO_CommonErrorLog
						.error("BLAD: TestA nie zostal zaliczony. Oczekiwano, ze zlecenia zostana obsluzone w czasie "
								+ estimatedExecutionTime * 1.1);
				PMO_CommonErrorLog.error("BLAD: TestA nie zostal zaliczony. Uzyskany wynik to : " + systemWorkTime);
				return false;
			}
		}

		PMO_SystemOutRedirect.println(
				"Zadanie zostalo wykonane w czasie " + systemWorkTime + " oczekiwano " + estimatedExecutionTime);

		boolean testResults = executeTests(allTestableObjects);
		return testResults;
	}

	@Override
	public long getRequiredTime() {
		return estimatedExecutionTime;
	}
}

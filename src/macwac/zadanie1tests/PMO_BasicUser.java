import java.util.Map;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Klasa bazowa do budowy uzytkownikow serwisu. Maja oni - dodawac roboty -
 * dodawac paczki - pobierac paczki
 * 
 * @author oramus
 *
 */
abstract class PMO_BasicUser implements PMO_RunnableAndTestable {
	protected int parcelID;
	protected int USid;
	protected ParcelInterface parcel;
	protected Map<Integer, Integer> parcelID2USid;
	protected CyclicBarrier beforeRealRun;
	protected CyclicBarrier afterRealRun;
	protected CyclicBarrier atTheEnd;
	protected static AtomicInteger helper = new AtomicInteger(0);
	protected static AtomicInteger helper2 = new AtomicInteger(0);

	protected long timeMeasured;

	protected BrokerInterface broker;
	protected AtomicBoolean runExecuted = new AtomicBoolean(false);

	public void setBroker(BrokerInterface broker) {
		this.broker = broker;
	}

	public void setBeforeRealRun(CyclicBarrier beforeRealRun) {
		this.beforeRealRun = beforeRealRun;
	}

	public void setAfterRealRun(CyclicBarrier afterRealRun) {
		this.afterRealRun = afterRealRun;
	}

	public void setAtTheEnd(CyclicBarrier atTheEnd) {
		this.atTheEnd = atTheEnd;
	}

	abstract protected void realRun();

	abstract protected boolean realTest();

	private void crossBarrier(CyclicBarrier cb) {
		try {
			if (cb != null)
				cb.await();
		} catch (InterruptedException | BrokenBarrierException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void println(String txt) {
		if (PMO_Consts.VERBOSE)
			PMO_SystemOutRedirect.println("[" + PMO_TimeHelper.getTimeFromStart() + "] "
					+ Thread.currentThread().getName() + " USER > " + txt);
	}

	@Override
	public void run() {		
		println( "Thread " + Thread.currentThread().getName() + " started # " + helper2.incrementAndGet() );
		
		crossBarrier(beforeRealRun);
		realRun();
		crossBarrier(afterRealRun);
		runExecuted.set(true);
		crossBarrier(atTheEnd);
	}

	@Override
	public boolean test() {
		if (!runExecuted.get()) {
			PMO_CommonErrorLog.error("BLAD: metoda run nie zostala zrealizowana przed testem");
			return false;
		}
		return realTest();
	}

}

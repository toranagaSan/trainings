import java.util.Collection;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Klasa do pomiaru czasu potrzebnego do wykonania wszystkich zadan.
 *
 */
public class PMO_AllTasksExecutionMeasurementTool {
	private Collection<Integer> tasksToDo;
	private long resolution;
	private AtomicLong result = new AtomicLong(-1);
	private Thread th;

	public PMO_AllTasksExecutionMeasurementTool(Collection<Integer> tasksToDo, long resolution) {
		this.resolution = resolution;
		this.tasksToDo = tasksToDo;
	}

	public void start() {
		th = new Thread() {
			@Override
			public void run() {
				result.set(PMO_TimeHelper.executionTime(() -> {
					return !tasksToDo.isEmpty();
				}, resolution));
			}
		};
		th.setDaemon(true);
		th.start();
	}

	public void start(CyclicBarrier cb) {
		th = new Thread() {
			@Override
			public void run() {
				try {
					cb.await();
				} catch (InterruptedException | BrokenBarrierException e) {
					e.printStackTrace();
				}
				result.set(PMO_TimeHelper.executionTime(() -> {
//					PMO_SystemOutRedirect.println( "tasksToDo = " + tasksToDo.size() );
					return !tasksToDo.isEmpty();
				}, resolution));
			}
		};
		th.setDaemon(true);
		th.start();
	}

	public long getResult() {
		return result.get();
	}
	
	public long getResult( long maxWait ) {
		try {
			th.join( maxWait );
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return result.get();
	}

}

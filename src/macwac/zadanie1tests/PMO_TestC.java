import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicBoolean;

public class PMO_TestC extends PMO_TestA {

	private AtomicBoolean parcelReceived = new AtomicBoolean(false);
	private int testVersion;

	@Override
	protected void setTestParameters() {
		robots = 3;

		parcels = robots + 1;

		parcelsToDeliver = robots / 2;
		parcelsToReceive = robots - robots / 2;

		USs = parcels;
		estimatedExecutionTime = 3000;

		slowDown = 1000000; // roboty nie dotra do US
	}

	@Override
	protected void init() {
		showTestParameters();
		createObjects();
		generateParcelsAndRoutes();
		generateUSAndRobots();
		setRoutesInUS();
		generateUsers();
		generateAll();
		setGlobalCollections();
		createAndSetBroker();
		setRobotSlowDown(0);
		setDisposableBarrier();
	}

	private Runnable getTest1(CyclicBarrier b) {

		return new Runnable() {
			public void run() {
				try {
					println("**** Test before await");
					b.await();
					println("**** Test behind await");
				} catch (Exception e) {
					e.printStackTrace();
				}
				PMO_TimeHelper.sleep(250); // 0.25 sekundy
				ParcelInterface testParcel = parcelsCollection.get(robots); // ostatnia
																			// paczka
				int parcelID = testParcel.getParcelID();
				int destination = parcelID2USid.get(parcelID);
				println("**** Push parcel id " + parcelID + " USdi " + destination);
				broker.push(testParcel, destination);
				PMO_TimeHelper.sleep(250); // 0.25 sekundy
				println("**** Peek parcel id " + parcelID + " USdi " + destination);
				ParcelInterface receivedParcel = broker.peek(destination, parcelID);
				println("**** Parcel id " + parcelID + " received from USid " + destination);
				if (receivedParcel == null) {
					PMO_CommonErrorLog.error("BLAD: zamiast paczki mamy null");
					return;
				}
				if (receivedParcel.getParcelID() == parcelID) {
					println("**** Parcel id " + parcelID + " received from USid " + destination + " OK");
					parcelReceived.set(true);
				} else {
					PMO_CommonErrorLog.error("BLAD: otrzymano paczke z blednym numerem id");
				}
			};
		};
	}

	private Runnable getTest2(CyclicBarrier b) {
		return new Runnable() {

			public void run() {
				try {
					println("**** Test before await");
					b.await();
					println("**** Test behind await");
				} catch (Exception e) {
					e.printStackTrace();
				}
				PMO_TimeHelper.sleep(250); // 0.25 sekundy
				ParcelInterface testParcel = parcelsCollection.get(robots); // ostatnia
																			// paczka
				int parcelID = testParcel.getParcelID();
				int destination = parcelID2USid.get(parcelID);

				Thread th = new Thread(new Runnable() {
					public void run() {
						PMO_TimeHelper.sleep(500); // 0.5 sekundy
						println("**** Push parcel id " + parcelID + " USdi " + destination);
						broker.push(testParcel, destination);
					};
				});
				th.start();

				println("**** Peek parcel id " + parcelID + " USdi " + destination);
				ParcelInterface receivedParcel = broker.peek(destination, parcelID);
				println("**** Parcel id " + parcelID + " received from USid " + destination);
				if (receivedParcel == null) {
					PMO_CommonErrorLog.error("BLAD: zamiast paczki mamy null");
					return;
				}
				if (receivedParcel.getParcelID() == parcelID) {
					println("**** Parcel id " + parcelID + " received from USid " + destination + " OK");
					parcelReceived.set(true);
				} else {
					PMO_CommonErrorLog.error("BLAD: otrzymano paczke z blednym numerem id");
				}
			};
		};
	}

	private Runnable getTest3(CyclicBarrier b) {
		return new Runnable() {

			public void run() {
				try {
					println("**** Test before await");
					b.await();
					println("**** Test behind await");
				} catch (Exception e) {
					e.printStackTrace();
				}
				PMO_TimeHelper.sleep(250); // 0.5 sekundy
				ParcelInterface testParcel = parcelsCollection.get(robots); // ostatnia
																			// paczka
				int parcelID = testParcel.getParcelID();
				int destination = parcelID2USid.get(parcelID);

				Thread th = new Thread(new Runnable() {
					public void run() {
						PMO_TimeHelper.sleep(500); // 0.5 sekundy
						println("**** Push parcel id " + parcelID + " USdi " + destination);
						broker.push(testParcel, destination);
					};
				});
				th.start();

				int wrongDestinationID = (destination + 1) % USs;
				println("**** Peek parcel id " + parcelID + " USdi " + wrongDestinationID);
				ParcelInterface receivedParcel = broker.peek(wrongDestinationID, parcelID);
				println("**** Parcel id " + parcelID + " received despite wrong USid");
				if (receivedParcel == null) {
					PMO_CommonErrorLog.error("BLAD: zamiast paczki mamy null");
					return;
				}
				if (receivedParcel.getParcelID() == parcelID) {
					println("**** Parcel id " + parcelID + " received from USid " + destination + " OK");
					parcelReceived.set(true);
				} else {
					PMO_CommonErrorLog.error("BLAD: otrzymano paczke z blednym numerem id");
				}
			};
		};
	}

	protected void setDisposableBarrier() {
		println("DisposableBarrier wymaga " + (1 + robots));
		CyclicBarrier b = new CyclicBarrier(robots + 1); // bariera ma
															// zablokowac roboty
		robotList.forEach(r -> r.setDisposableBarrier(b));

		Runnable code = null;
		if (testVersion == 1) {
			PMO_SystemOutRedirect.println("**** Najpierw paczka potem jej odczyt");
			code = getTest1(b);
		}
		if (testVersion == 2) {
			PMO_SystemOutRedirect.println("**** Najpierw odczyt a potem dodanie poszukiwanej paczki");
			code = getTest2(b);
		}
		if (testVersion == 3) {
			PMO_SystemOutRedirect.println("**** Najpierw odczyt a potem dodanie poszukiwanej paczki. Rozne (bledne) USid");
			code = getTest3(b);
		}
		Thread th = new Thread(code);
		th.setName("TEST-THREAD");
		th.setDaemon(true);
		th.start();
		new PMO_ThreadWatcher(th).start(1500);
	}

	public PMO_TestC(int testVersion) {
		PMO_SystemOutRedirect.println("TEST C_C_C_C_C");
		this.testVersion = testVersion;
		setTestParameters();
	}

	@Override
	public void run() {
		init();

		PMO_SystemOutRedirect.println("Paczki    : " + parcelsCollection.size());
		PMO_SystemOutRedirect.println("Roboty    : " + robotList.size());
		PMO_SystemOutRedirect.println("USs       : " + USlist.size());
		PMO_SystemOutRedirect.println("Runnable  : " + allRunnubleObjects.size());
		PMO_SystemOutRedirect.println("ToDo list : " + toDo.size());

		PMO_ThreadsHelper.createAndStartThreads(allRunnubleObjects, true);
	}

	@Override
	public boolean test() {
		return parcelReceived.get();
	}

}

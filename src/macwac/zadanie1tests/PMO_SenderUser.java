import java.util.concurrent.atomic.AtomicInteger;

public class PMO_SenderUser extends PMO_BasicUser {

	private boolean timeMeasureValid;
	private static AtomicInteger localHelper = new AtomicInteger(0);

	public PMO_SenderUser(ParcelInterface parcel, int USid, boolean timeMeasureValid) {
		if (parcel == null) {
			PMO_SystemOutRedirect.println("Internal error: PMO_SenderUser otrzymymal null zamiast paczki");
			PMO_CommonErrorLog.criticalMistake();
		}
		this.parcel = parcel;
		this.USid = USid;
		this.timeMeasureValid = timeMeasureValid;
	}

	@Override
	protected void realRun() {
		timeMeasured = PMO_TimeHelper.executionTime(() -> {
			broker.push(parcel, USid);
		});
		println("PMO_Sender: push [" + localHelper.incrementAndGet() + "/"
				+ helper.incrementAndGet() + "] paczka ID : " + parcel.getParcelID());
	}

	protected boolean realTest() {
		if (timeMeasureValid) {
			if (timeMeasured > PMO_Consts.MAX_PUSH_PARCEL_EXEC_TIME) {
				PMO_SystemOutRedirect.println("BLAD: zbyt dlugi czas wykonania broker.push " + timeMeasured + " msec");
				return false;
			}
		}
		return true;
	}
}


public class PMO_Consts {
	public static final long MAX_PUSH_PARCEL_EXEC_TIME = 50;
	public static final long MAX_ROBOT_ADD_TIME = 50;
	public static final long TIME_MEASURE_RESOLUTION = 33;
	public static final boolean VERBOSE = true;
	public static final long ROBOT_BAN = 350; // msec
}

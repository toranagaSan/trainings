public class PMO_Start {
	private static boolean runTest(PMO_RunTestTimeout test) {
		long timeToFinish = 2 * test.getRequiredTime();
		Thread th = new Thread(test);
		th.setDaemon(true);
		th.start();

		PMO_SystemOutRedirect.println("timeToFinish = " + timeToFinish);
		PMO_SystemOutRedirect.println("Maksymalny czas oczekiwania to " + (timeToFinish / 1000) + " sekund");

		long beforeJoin = System.currentTimeMillis();
		try {
			th.join(timeToFinish);
		} catch (InterruptedException e) {
		}
		long remainingTime = timeToFinish - System.currentTimeMillis() + beforeJoin;
		if (remainingTime > 0) {
			PMO_SystemOutRedirect.println("Dodatkowy czas: " + remainingTime + " msec");
			PMO_TimeHelper.sleep(remainingTime);
		}

		PMO_SystemOutRedirect.println("Zakonczyl sie czas oczekiwania na join()");

		if (th.isAlive()) {
			PMO_SystemOutRedirect.println("BLAD: Test nie zostal ukonczony na czas");
			return false;
		} else {
			PMO_SystemOutRedirect.println("Uruchamiam test");
			return test.test();
		}

	}

	private static void shutdownIfFail(boolean testOK) {
		if (!testOK) {
			PMO_Verdict.show(false);
			shutdown();
		}
	}

	private static void shutdown() {
		System.out.println("HALT");
		Runtime.getRuntime().halt(0);
		System.out.println("EXIT");
		System.exit(0);
	}

	public static void main(String[] args) {

		final int REPETITIONS_A = 6;
		final int REPETITIONS_B = 4;
		final int REPETITIONS_C = 4;

		PMO_SystemOutRedirect.startRedirectionToNull();

		PMO_UncaughtException uncaughtExceptionsLog = new PMO_UncaughtException();

		boolean result = true;

		for (int i = 0; (i < REPETITIONS_A) && (result); i++) {
			result &= runTest(new PMO_TestA());
			result &= PMO_CommonErrorLog.isStateOK();
			System.err.println("REPETITIONS_A");
		}
		for (int i = 0; (i < REPETITIONS_B) && (result); i++) {
			result &= runTest(new PMO_TestB());
			result &= PMO_CommonErrorLog.isStateOK();
			result &= runTest(new PMO_TestB_Parallel());
			result &= PMO_CommonErrorLog.isStateOK();
			System.err.println("REPETITIONS_B");
		}
		for (int i = 0; (i < REPETITIONS_C) && (result); i++) {
			result &= runTest(new PMO_TestC(2));
			result &= PMO_CommonErrorLog.isStateOK();
			System.err.println("REPETITIONS_C v1");
		}

		for (int i = 0; (i < REPETITIONS_C) && (result); i++) {
			result &= runTest(new PMO_TestC(3));
			result &= PMO_CommonErrorLog.isStateOK();
			System.err.println("REPETITIONS_C v2");
		}

		for (int i = 0; (i < REPETITIONS_C) && (result); i++) {
			result &= runTest(new PMO_TestC(1));
			result &= PMO_CommonErrorLog.isStateOK();
			System.err.println("REPETITIONS_C v3");
		}

		if (!result) {
			PMO_SystemOutRedirect.println("--- log bledow ---");
			PMO_CommonErrorLog.getErrorLog(0).forEach(PMO_SystemOutRedirect::println);
		}

		if (!uncaughtExceptionsLog.isEmpty()) {
			PMO_SystemOutRedirect.println("--- log wyjatkow ---");
			PMO_SystemOutRedirect.println(uncaughtExceptionsLog.toString());
		}

		PMO_Verdict.show(result);

		shutdown();
	}
}

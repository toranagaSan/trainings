import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicBoolean;

public class PMO_TestB extends PMO_TestA {

	private int robotsWithProblem;
	private int okRobots;
	private AtomicBoolean testStarted = new AtomicBoolean(false);

	@Override
	protected void setTestParameters() {
		robots = (8 + rnd.nextInt(6)) * 2;

		robotsWithProblem = robots / 2 + 1;
		okRobots = robots - robotsWithProblem;
		parcelsToDeliver = okRobots;
		parcelsToReceive = robotsWithProblem;

		parcels = robotsWithProblem * 2 + okRobots;

		USs = parcels;
		estimatedExecutionTime = 5000;
		
		slowDown = 250;
	}

	protected void markDamagedRobots() {
		ParcelInterface parcel;
		int destinationUS;
		AtomicBoolean chosenOne = new AtomicBoolean( false );
		for (int i = 0; i < robotsWithProblem; i++) {
			parcel = parcelsCollection.get(parcels - 1 - i);
			destinationUS = parcelID2USid.get(parcel.getParcelID());
			robotList.get(i).setParcelToAddAfterAccident(parcel, destinationUS, broker, chosenOne, null, null );
			robotList.get(i).setToDo(toDo);
			toDo.remove( parcel.getParcelID() );
		}
	}

	@Override
	protected void init() {
		showTestParameters();
		createObjects();
		generateParcelsAndRoutes();
		generateUSAndRobots();
		setRoutesInUS();
		generateUsers();
		generateAll();
		setGlobalCollections();
		createAndSetBroker();
		markDamagedRobots();
		setDisposableBarrier();
		setUnfinishedJobsMap();
		setRobotSlowDown(0);
	}

	public PMO_TestB() {
		PMO_SystemOutRedirect.println("TEST B_B_B_B_B");
		setTestParameters();
	}

	private void setUnfinishedJobsMap() {
		Map<Integer, Integer> unfinishedJobs = Collections.synchronizedMap(new TreeMap<>());
		robotList.forEach(r -> r.setUnfinishedJobs(unfinishedJobs));
	}

	protected void setDisposableBarrier() {
		CyclicBarrier b = new CyclicBarrier(robots + 1);

		PMO_SystemOutRedirect.println("DisposableBarrier wymaga " + (1 + robots));

		Thread th = new Thread(() -> {
			try {
				b.await();
			} catch (Exception e) {
				e.printStackTrace();
			}
			testStarted.set(true);
		});
		th.setDaemon(true);
		th.start();

		robotList.forEach(r -> r.setDisposableBarrier(b));
	}

	@Override
	public void run() {
		init();

		PMO_SystemOutRedirect.println("Paczki    : " + parcelsCollection.size());
		PMO_SystemOutRedirect.println("Roboty    : " + robotList.size());
		PMO_SystemOutRedirect.println("USs       : " + USlist.size());
		PMO_SystemOutRedirect.println("Runnable  : " + allRunnubleObjects.size());
		PMO_SystemOutRedirect.println("ToDo list : " + toDo.size());

		PMO_ThreadsHelper.createAndStartThreads(allRunnubleObjects, true);
	}

	@Override
	public boolean test() {

		PMO_SystemOutRedirect.println("Zadania do realizacji toDO " + toDo.size());
		if ( ! toDo.isEmpty() ) {
			PMO_CommonErrorLog.error( "BLAD: Test B nie zostal zaliczony. Nie zrealizowano zleconych prac");
		}
		if ( ! testStarted.get() ) {
			PMO_CommonErrorLog.error( "BLAD: Test B nie zostal zaliczony. Stan robotow nie pozwolil na jego przeprowadzenie");
		}

		return testStarted.get() && toDo.isEmpty();
	}
}

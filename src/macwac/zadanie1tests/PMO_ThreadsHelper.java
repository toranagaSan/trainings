import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PMO_ThreadsHelper {
	public static void joinThreads(List<Thread> ths) {
		ths.forEach((t) -> {
			try {
				t.join();
			} catch (InterruptedException ie) {
				PMO_SystemOutRedirect.println("Doszlo do wyjatku w trakcie join");
			}
		});
	}
	
	public static List<Thread> createAndStartThreads( Collection<? extends Runnable> tasks, boolean daemon ) {
		List<Thread> result = new ArrayList<>();
		
		tasks.forEach( t -> {
			result.add( new Thread( t ) ) ;
		});
		
		if ( daemon ) {
			result.forEach( t -> t.setDaemon( true ) );
		}
		
		result.forEach( t -> t.start() );
		
		return result;
	}

}

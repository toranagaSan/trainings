import java.util.function.BooleanSupplier;

public class PMO_TimeHelper {
	
	private static long t0 = System.currentTimeMillis();

	public interface PeriodOfTime {
		void updateIfBigger(long value);

		long getValue();
	}

	private static PeriodOfTime commonPOTime;

	public static PeriodOfTime getPeriodOfTimeTool() {
		return new PeriodOfTime() {

			private long value = 0;

			@Override
			synchronized public long getValue() {
				return value;
			}

			@Override
			synchronized public void updateIfBigger(long value) {
				if (value > this.value) {
					this.value = value;
				}
			}
		};
	}

	public static void createCommonPeriodOfTimeTool() {
		commonPOTime = getPeriodOfTimeTool();
	}

	public static PeriodOfTime getCommonPeriodOfTimeTool() {
		return commonPOTime;
	}

	public static long executionTime(Runnable run) {
		long start = System.currentTimeMillis();

		run.run();

		return System.currentTimeMillis() - start;
	}

	public static long executionTime(BooleanSupplier run, long resolution) {
		long start = System.currentTimeMillis();

		while (run.getAsBoolean()) {
			sleep(resolution);
		}

		return System.currentTimeMillis() - start;
	}

	public static boolean sleep(long millis) {
		long tf = getCurrentTime() + millis;
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
			PMO_CommonErrorLog.error( "W trakcie wykonywania sleep doszlo do wyjatku InterruptedException");
			do {
				try {
					Thread.sleep(1);
				} catch (InterruptedException e1) {
//					e1.printStackTrace();
				}
			} while ( getCurrentTime() < tf );
			
			return false;
		}
		return true;
	}

	public static long getCurrentTime() {
		return System.currentTimeMillis();
	}
	
	public static long getTimeFromStart() {
		return System.currentTimeMillis() - t0;
	}
}

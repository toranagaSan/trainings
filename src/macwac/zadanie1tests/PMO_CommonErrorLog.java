import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class PMO_CommonErrorLog {
	private static boolean stateOK = true;
	private static boolean criticalMistake = false;
	private static Queue<String> errorLog = new LinkedList<>();

	synchronized public static boolean isStateOK() {
		return stateOK;
	}

	synchronized public static boolean isCriticalMistake() {
		return criticalMistake;
	}

	synchronized public static void error(String description) {
		stateOK = false;
		errorLog.add(PMO_TimeHelper.getTimeFromStart() + "> " + description);
		PMO_SystemOutRedirect.println("*" + PMO_TimeHelper.getTimeFromStart() + "> "
				+ Thread.currentThread().getName() + ": " + " " + description);
	}

	synchronized public static List<String> getErrorLog(int size) {
		return new ArrayList<>(errorLog);
	}

	synchronized public static void criticalMistake() {
		criticalMistake = true;
		PMO_SystemOutRedirect.println("              ---- --- ------ --- ----");
		PMO_SystemOutRedirect.println("              ---- BLAD KRYTYCZNY ----");
		PMO_SystemOutRedirect.println("              ---- BLAD KRYTYCZNY ----");
		PMO_SystemOutRedirect.println("              ---- --- ------ --- ----");
		PMO_SystemOutRedirect.println("Zgłoszono błąd krytyczny, kontynuacja testu nie ma sensu");
		PMO_SystemOutRedirect.println("Poniżej pojawi się log błędów po jego wyświtleniu podjęta");
		PMO_SystemOutRedirect.println("zostanie próba wyłącznia JVM.");

		errorLog.forEach(System.out::println);

		System.out.println("EXIT");
		System.exit(0);
		System.out.println("HALT");
		Runtime.getRuntime().halt(0);
	}

}

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class PMO_US implements PMO_Testable {
	// numer ID tego US
	private final int id;

	// paczki w tym US
	private Map<Integer, ParcelInterface> parcelsInUS = Collections.synchronizedMap(new HashMap<>());
	// paczki przekazane do tego US
	private Set<Integer> parcelsDelivered2US = Collections.synchronizedSet(new HashSet<>());

	// globalny zbior paczek przekazanych do US - wykrywa powielenia paczki
	private Set<Integer> globalSetOfParcelsDelivered2US;

	// globalny zbior paczek do zgubienia
	private Set<Integer> globalSetOfParcels2Lose;

	// globalny zbior paczek u US i uzytkownikow
	private Set<Integer> globalSetOfParcelsInUSorInUser;

	// globalna mapa tras paczek
	private Map<Integer, Integer> parcelID2USid;

	// maksymalna liczba robotow jednoczesnie wyslanych do tego US
	private PMO_AtomicCounter maxRobots = PMO_CountersFactory.prepareCommonMaxStorageCounter();

	// aktualna liczba robotow skierownych do tego US
	private PMO_AtomicCounter robots = PMO_CountersFactory.prepareCounterWithMaxStorageSet();

	// globalny zbior zadan, ktore maja sie jeszcze wykonac
	private Collection<Integer> toDo;

	public void setGlobalSetOfParcelsDelivered2US(Set<Integer> globalSetOfParcelsDelivered2US) {
		this.globalSetOfParcelsDelivered2US = globalSetOfParcelsDelivered2US;
	}

	public void setGlobalSetOfParcels2Lose(Set<Integer> globalSetOfParcels2Lose) {
		this.globalSetOfParcels2Lose = globalSetOfParcels2Lose;
	}

	public void setGlobalSetOfParcelsInUSorInUser(Set<Integer> globalSetOfParcelsInUSorInUser) {
		this.globalSetOfParcelsInUSorInUser = globalSetOfParcelsInUSorInUser;
	}

	public void setToDo(Collection<Integer> toDo) {
		this.toDo = toDo;
	}

	public PMO_AtomicCounter getRobotsCounter() {
		return robots;
	}

	public void setParcel2USMap(Map<Integer, Integer> parcelID2USid) {
		this.parcelID2USid = parcelID2USid;
	}

	public PMO_US(int id) {
		this.id = id;

		maxRobots.setFailPredicate(PMO_IntPredicateFactory.moreThenOne());

	}

	// metoda do dokladania paczek przed uruchomieniem symulacji
	public void preAdd(ParcelInterface p) {
		parcelsInUS.put(p.getParcelID(), p);
	}

	public void add(ParcelInterface p) {

		assert (parcelID2USid != null) : "parcelID2USid = null!";
		assert (parcelsDelivered2US != null) : "parcelsDelivered2US = null ";
		assert (globalSetOfParcelsDelivered2US != null) : "globalSetOfParcelsDelivered2US = null";
		assert (globalSetOfParcelsInUSorInUser != null) : "globalSetOfParcelsInUSorInUser = null";
		assert (globalSetOfParcels2Lose != null) : "globalSetOfParcels2Lose = null";
		assert (toDo != null) : "toDo = null";

		if (p == null) {
			PMO_CommonErrorLog.error("Do US " + id + " dotarla paczka == null");
			return;
		}

		if (id != parcelID2USid.get(p.getParcelID())) {
			PMO_CommonErrorLog.error(
					"Paczka dotarla do zlego US, powinno byc " + parcelID2USid.get(p.getParcelID()) + " a jest " + id);
		}

		if (!parcelsDelivered2US.add(p.getParcelID())) {
			PMO_CommonErrorLog.error("Wykryto powielenie paczki " + p.getParcelID() + " w US " + id);
			PMO_CommonErrorLog.criticalMistake();
		}

		if (!globalSetOfParcelsDelivered2US.add(p.getParcelID())) {
			PMO_CommonErrorLog.error("Wykryto powielenie paczki " + p.getParcelID() + " wsrod US ");
			PMO_CommonErrorLog.criticalMistake();
		}

		if (!globalSetOfParcelsInUSorInUser.add(p.getParcelID())) {
			PMO_CommonErrorLog.error("Wykryto powielenie paczki pomiedzy US i uzytkownikiem " + p.getParcelID());
			PMO_CommonErrorLog.criticalMistake();
		}

		if (!globalSetOfParcels2Lose.contains(p.getParcelID())) {
			// paczka nie zostala zgubiona
			parcelsInUS.put(p.getParcelID(), p);
		}

//		PMO_SystemOutRedirect.println("add parcel " + p.getParcelID());
		toDo.remove(p.getParcelID());
	}

	public ParcelInterface get(int id) {
		ParcelInterface p = parcelsInUS.get(id);
		if (p != null) {
			parcelsInUS.remove(p);
		}

		// paczka zostala dostarczona, choc mogla zaginac
		if (parcelsDelivered2US.contains(id)) {
			globalSetOfParcelsInUSorInUser.remove(id);
		}

//		PMO_SystemOutRedirect.println("get parcel " + id);
		toDo.remove(p.getParcelID());
		toDo.remove(id);

		return p;
	}

	@Override
	public boolean test() {

		boolean result = true;

		if (maxRobots.isFail().get()) {
			PMO_CommonErrorLog
					.error("US " + id + " wykryl, ze skierowano do niego wiecej niz jednego robota jednoczesnie");
			PMO_CommonErrorLog
					.error("US " + id + " byl obslugiwany rownoczesnie przez " + maxRobots.get() + " roboty");
			PMO_CommonErrorLog.criticalMistake();
			result = false;
		}

		return result;
	}

}

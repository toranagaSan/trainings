import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
 
public class PMO_Factory {
	private List<PMO_Robot> robots = new ArrayList<>();
	private List<PMO_US> USs = new ArrayList<>();
	private PMO_AtomicCounter[] UScounters;
	private AtomicInteger parcelID = new AtomicInteger();
	private AtomicBoolean robotsCanNotMove;

	public AtomicBoolean getRobotsCanNotMove() {
		return robotsCanNotMove;
	}

	// tworzony jest watek, ktory zostanie zaktywowany za pomoca bariery
	public void startRobotsTriggerThread(CyclicBarrier cb) {
		Thread th = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					cb.await();
				} catch (InterruptedException | BrokenBarrierException e) {
					e.printStackTrace();
				}

				synchronized (robotsCanNotMove) {
					robotsCanNotMove.set(false);
					robotsCanNotMove.notifyAll();
				}
			}
		});
		th.setDaemon(true);
		th.start();
	}

	public List<PMO_Robot> getRobots() {
		return robots;
	}

	public List<PMO_US> getUSs() {
		return USs;
	}

	public void createUSandRobots(int USs, int robots, boolean canMove) {
		UScounters = new PMO_AtomicCounter[USs];
		robotsCanNotMove = new AtomicBoolean(!canMove);
		PMO_US us;
		for (int i = 0; i < USs; i++) {
			us = new PMO_US(i);
			this.USs.add(us);
			UScounters[i] = us.getRobotsCounter();
		}
		PMO_Robot robot;
		for (int i = 0; i < robots; i++) {
			robot = new PMO_Robot(UScounters, this.USs, robotsCanNotMove);
			this.robots.add(robot);
		}
	}

	public ParcelInterface createNewParcel() {
		final int i = parcelID.incrementAndGet();
		return new ParcelInterface() {
			private final int id = i;

			@Override
			public int getParcelID() {
				return id;
			}
		};
	}
}


public class PMO_GeneralPurposeFabric {
	public static Object fabric(String className, String interfaceName) {
		try {
			if (Class.forName(interfaceName).isAssignableFrom(Class.forName(className))) {
				return Class.forName(className).newInstance();
			} else {
				PMO_SystemOutRedirect
						.println("Blad: klasa " + className + " nie jest zgodna z interfejsem " + interfaceName);
				System.exit(1);
			}
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		System.exit(1);
		return null;
	}

}

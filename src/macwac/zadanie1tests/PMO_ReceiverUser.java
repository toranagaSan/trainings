import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class PMO_ReceiverUser extends PMO_BasicUser {

	private ParcelInterface expected;
	// globalny zbior paczek u US i uzytkownikow
	private Set<Integer> globalSetOfParcelsInUSorInUser;
	private static AtomicInteger localHelper = new AtomicInteger(0);

	public void setGlobalSetOfParcelsInUSorInUser(Set<Integer> globalSetOfParcelsInUSorInUser) {
		this.globalSetOfParcelsInUSorInUser = globalSetOfParcelsInUSorInUser;
	}

	public PMO_ReceiverUser(int parcelID, int USid, ParcelInterface expected) {
		this.parcelID = parcelID;
		this.USid = USid;
		this.expected = expected;
	}

	@Override
	protected void realRun() {
		println("PMO_ReceiverUser: peek [" + localHelper.incrementAndGet() + "/"
				+ helper.incrementAndGet() + "] paczka ID : " + parcelID);
		parcel = broker.peek(USid, parcelID);

		println("Paczke odebrano !!!!!!!!!!!!!!!");

		if (parcel != null)

			if (!globalSetOfParcelsInUSorInUser.add(parcel.getParcelID())) {
				PMO_CommonErrorLog
						.error("Wykryto powielenie paczki pomiedzy US i uzytkownikiem " + parcel.getParcelID());
				PMO_CommonErrorLog.criticalMistake();
			}
	}

	@Override
	protected boolean realTest() {
		if (expected == null) {
			if (parcel != null) {
				PMO_CommonErrorLog
						.error("BLAD: oczekiwano, ze paczki " + parcelID + " w " + USid + " nie bedzie (null)");
				return false;
			}
		} else {
			if (parcel == null) {
				PMO_CommonErrorLog
						.error("BLAD: oczekiwano, ze paczka " + parcelID + " w " + USid + " bedzie, a jest null");
				return false;
			}
			if (parcel.getParcelID() != parcelID) {
				PMO_CommonErrorLog.error("BLAD: oczekiwano paczki z " + parcelID + " w " + USid + " a otrzymano paczke "
						+ parcel.getParcelID());
				return false;
			}
		}
		return true;
	}

}

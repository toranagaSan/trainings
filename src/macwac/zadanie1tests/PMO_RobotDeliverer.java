
public class PMO_RobotDeliverer extends PMO_BasicUser {

	private PMO_Robot robot;
	private boolean timeMeasureValid;

	public PMO_RobotDeliverer(PMO_Robot robot, boolean timeMeasureValid) {
		assert (robot != null) : "Internal error: PMO_RobotDeliverer otrzymymal null zamiast robota";
		this.robot = robot;
		this.timeMeasureValid = timeMeasureValid;
	}

	@Override
	protected void realRun() {
		timeMeasured = PMO_TimeHelper.executionTime(() -> broker.addRobot(robot));
		println("PMO_RobotDeliverer: add " + helper.incrementAndGet() + " dostarczono robota " + robot.getRobotID());
	}

	@Override
	protected boolean realTest() {
		if (timeMeasureValid) {
			if (timeMeasured > PMO_Consts.MAX_ROBOT_ADD_TIME) {
				PMO_SystemOutRedirect
						.println("BLAD: zbyt dlugi czas wykonania broker.addRobot " + timeMeasured + " msec");
				return false;
			}
		}
		return true;

	}

}

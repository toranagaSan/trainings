W tym zadaniu trzeba stworzyć oprogramowanie zarządzające zrobotyzowanym systemem przechowywania paczek.

W systemie znajduje się określona i stała liczba urządzeń składujących (US). Użytkownicy przekazują do przechowania w US paczki, zlecają też ich pobranie. Paczki wprowadzane są i pobierane z US za pomocą autonomicznych robotów. Pośrednikiem pomiędzy użytkownikami a robotami jest Państwa oprogramowanie. To do niego użytkownicy przekazują paczki, to ono zarządza ruchem robotów i to ono zwraca użytkownikom paczki, o które poproszą.

Oczywiście, system musi działać współbieżnie. Wielu użytkowników może jednocześnie używać systemu. Wiele robotów może równocześnie wykonywać powierzone im zadania. W trakcie pracy systemu roboty mogą ulegać awarii. Mogą być także wprowadzane nowe roboty.

###############################################

W tym zadaniu trzeba stworzyć oprogramowanie zarządzające zrobotyzowanym systemem przechowywania paczek.

W systemie znajduje się określona i stała liczba urządzeń składujących (US). Użytkownicy przekazują do przechowania w US paczki, zlecają też ich pobranie. Paczki wprowadzane są i pobierane z US za pomocą autonomicznych robotów. Pośrednikiem pomiędzy użytkownikami a robotami jest Państwa oprogramowanie. To do niego użytkownicy przekazują paczki, to ono zarządza ruchem robotów i to ono zwraca użytkownikom paczki, o które poproszą.

Oczywiście, system musi działać współbieżnie. Wielu użytkowników może jednocześnie używać systemu. Wiele robotów może równocześnie wykonywać powierzone im zadania. W trakcie pracy systemu roboty mogą ulegać awarii. Mogą być także wprowadzane nowe roboty.

Uwagi dotyczące pracy systemu:

jeden robot może w danej chwili wykonywać tylko jedno zlecenie. --> CachedThreadPoolExecutor guarantee this
Może przenosić paczkę do US, albo ją stamtąd pobierać - ale nie równocześnie! ConcurrentHashMap guarantee this
Przekazanie robotowi nowego zlecenia, w trakcie obsługi poprzedniego, to błąd krytyczny. --> CachedThreadPoolExecutor
jedno US może być w danej chwili obsługiwane tylko przez jednego robota. --> ConcurrentHashMap  
Zlecenie dwóm robotom wykonywania dowolnej pracy na tym samym US, to błąd krytyczny. --> it does not happen, ThreadPoolExecutor
jednoczesne zleceniem dwóm robotom tego samego zadania, to błąd krytyczny. --> same as below
użytkownik nie może czekać na to, aż jego paczka dotrze do US. --> threads, daemons
Paczki przekazane przez użytkownika muszą być w systemie "buforowane".   TODO  
Z punktu widzenia użytkownika prowadzenie do systemu paczki jest czynnością natychmiastową --> TODO loop for reading new orders 
Zmuszenie użytkowników systemu do oczekiwania na umieszczenie w US paczek jest niedopuszczalne. 
    --> problem only when robot started execution (e.g. pushing parcel). TODO implement notifying mechanizm
    
Roboczo można założyć, że średni czas umieszczenia paczki w systemie (nie w US) powinien być na poziomie do 10-20 milisekund. --> so what?

bufor musi być inteligentny. 
Użytkownik chcący pobrać paczkę, która jeszcze jest w buforze nie może na nią czekać, a paczka nie może odbyć drogi do US i z US. --> priorityBuffer
 
Jeśli jednak paczka już została przekazana z bufora robotowi, to musi odbyć pełen cykl umieszczenia i pobrania jej z US. Czyli, ta sama paczka nie może być jednocześnie u użytkownika i u robota (a w konsekwencji w US).
użytkownik musi czekać na odebranie paczki. Wykonanie metody peek nie może się zakończyć wcześniej niż, gdy bufor lub roboty wraz z US, dostarczą paczkę lub informację, że takiej w danym US nie ma.
jedynym miejscem, w którym może być tymczasowo przechowywana wiedza o paczkach jest bufor. Poza nim oprogramowanie zarządzające nie może przechowywać informacji o paczkach. Jeśli paczka nie jest w buforze, to musi być odebrana (o ile istnieje) z US. Można założyć, że US mogą zawierać paczki jeszcze sprzed uruchomienia Państwa systemu, oraz, że przekazane im paczki mogą zginąć. Aby ustalić czy dana paczka jest w US, czy jej w nim nie ma, trzeba wysłać robota!
paczki w Państwa systemie nie mogą ginąć (jeśli gdzieś zginą to tylko wewnątrz US), ani powielać się. Roboty będą działać poprawnie, ale trzeba nimi poprawnie zarządzać i przekazywać im właściwe dane. Zachowanie robota, któremu powierzona zostanie paczka do umieszczenia w US o błędnym numerze może być dowolne. Ale podkreślam, że sam użytkownik zawsze będzie używał właściwych numerów USid.
dodanie robota do systemu musi być operacją praktycznie natychmiastową. Dodanie robota nie może być wstrzymywane przez oczekiwanie na pojawienie się w systemie paczki czy powrót innego robota z powierzonej mu misji.
W tej samej chwili wiele wątków może wykonać metodę dodającą robota. Robotów nie można zgubić!
dodany do systemu nowy robot musi otrzymać zlecenie wykonania pracy natychmiast, gdy tylko zaistnieje taka możliwość. Podobnie, istniejące w systemie roboty muszą otrzymywać zadania gdy tylko pojawia się taka możliwość - system nie może rozdzielać pracy tylko np. z okazji zakończenia obsługi wcześniejszego zadania. System musi wykorzystywać optymalnie zasoby (roboty, paczki i zbiór urządzeń składujących) i realizować tyle prac równocześnie, ile to jest możliwe.
czasy wykonania zlecenia przez roboty mogą się zmieniać. Mogą być też różne dla różnych robotów. Pomimo tego, system musi optymalnie zarządzać robotami.
uszkodzonemu robotowi nie wolno przekazywać nowych zadań. Jeśli wykonanie metody peek zakończy się zgłoszeniem wyjątku przez robota, to robot ten nie ma prawa uzyskać jakiegokolwiek kolejnego zlecenia. Zlecenie pobrania paczki należy przekazać kolejnemu robotowi natychmiast lub gdy tylko taki będzie dostępny. Zlecenia już rozpoczęte (zlecenie pobranie paczki z US) mają priorytet nad nowymi. Jeśli system będzie mieć do dyspozycji robota, któremu można powirzyć nowe zadanie i zadanie, które nie zostało dokończone z powodu awarii robota, to należy przydzielić do drugie.
System ma działać efektywnie gospodarując zasobami CPU. Używanie procesora w czasie bezczynności (brak zleceń od użytkowników i brak paczek w buforze) jest zabronione - watki, które nie mają co robić powinny być usypianie.
System musi zachowywać się poprawnie nawet w przypadku dużej liczby zleceń. Czasy kolejkowania zleceń klientów nie mogą znacząco rosnąć wraz z liczbą oczekujących na wykonanie operacji na paczkach czy robotów. Chodzi tu o to, aby rozwiązania, które będą tworzyć duże ilości wątków efektywne je blokowały. Usypianie dużej ilości wątków za pomocą jednego obiektu i budzone ich wszystkich przy okazji każdej zmiany w systemie jest zdecydowanie złym rozwiązaniem.
Gwarancje

Nigdy zamiast robota czy paczki nie pojawi się null
Numer US (USid) zawsze będzie miał poprawną wartość [od 0 do (liczba US-1)], choć w przypadku metody peek może się okazać, że poszukiwanej paczki w danym US nie będzie
Roboty, które zostaną oddane do dyspozycji Państwa systemu są zawodne. Może się tak zdarzyć, że ulegną awarii (zlecenie kończy się wyjątkiem). Na szczęście awaria może się przytrafić robotowi tylko w drodze po paczkę do US. Robot z paczką samorzutnie nigdy nie ulegnie awarii.
Metoda setNumberOfStorageUnits na pewno zostanie wywołana zgodnie z jej opisem i przekazana liczba US będzie większa od 0.
Numery identyfikacyjne różnych paczek będą różne. Różne paczki będą reprezentowane przez różne obiekty.
Nowy robot będzie faktycznie nowym robotem-obiektem. Każdy robot to inny obiekt. Metoda addRobot zawsze będzie przekazywać inny obiekt.
Dostarczenie rozwiązania.

Rozwiązanie proszę dostarczyć w postaci klasy o nazwie Broker. Klasa ma implementować interfejs BrokerInterface 
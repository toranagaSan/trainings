package macwac.zadanie1;

import javafx.concurrent.Task;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.logging.Logger;

/**
 * @author Pawel Tarsa (SG0953429)
 * @since 4/8/2017.
 */
public class FinalBroker extends ThreadPoolExecutor implements BrokerInterface {

    public static final Logger logger = Logger.getLogger(Broker.class.getSimpleName());
    private ConcurrentHashMap<Integer, List<ParcelInterface>> storageUnitRegister = new ConcurrentHashMap<>();
    private ConcurrentLinkedDeque<RobotInterface> robotRegister = new ConcurrentLinkedDeque<>();
    private ConcurrentLinkedDeque<ParcelInterface> brokenParcelRegister = new ConcurrentLinkedDeque<>();
//    private PriorityBlockingQueue<Runnable> priorityBuffer;  // there can be brokenParcelRegister included

    public FinalBroker(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, new RejectedExecutionHandler() {
            @Override
            public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                synchronized (this) {
                    try {
                        logger.info("No available robots, task pushed into execution queue");
                        executor.getQueue().put(r);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        });
    }

    public static void main(String... args) {
        FinalBroker broker = new FinalBroker(0, 10, 60,
                TimeUnit.SECONDS, new PriorityBlockingQueue<>());
        broker.addRobot(new RobotImpl());
        broker.push(new ParcelImpl(1), 1);
        ParcelInterface p = broker.peek(1, 1);
    }

    @Override
    public void beforeExecute(Thread t, Runnable r) {
    }

    @Override
    public void afterExecute(Runnable r, Throwable t) {

    }


    @Override
    public void setNumberOfStorageUnits(int USs) {
        if (storageUnitRegister.get(USs) == null) {
            storageUnitRegister.put(USs, new ArrayList<>());
        } else {
            logger.warning(String.format("US: %d exists in US register. ", USs));
        }
    }

    @Override
    public void addRobot(RobotInterface robot) {
        synchronized (this) {
            robotRegister.add(robot);
            this.setCorePoolSize(robotRegister.size());
        }
    }

    @Override
    public void push(ParcelInterface parcel, int USid) {
        this.submit(new PushParcelTask(parcel, USid, robotRegister.remove()));
    }

    @Override
    public ParcelInterface peek(int USid, int parcelID) {
        Future<ParcelTask> parcelTask = (Future<ParcelTask>) this.submit(new PeekParcelTask(USid, parcelID, robotRegister.remove()));
        try {
            return parcelTask.get().getParcel();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    private interface ComparableTask<E extends ParcelTask> {
        default int compareTo(E o1, E o2) {
            if (o1.getPriority() > o2.getPriority())
                return 1;
            else if (o1.getPriority() == o2.getPriority())
                return 0;
            return -1;
        }
    }

    private abstract class ParcelTask extends Task<ParcelInterface> implements ComparableTask, Comparable<ParcelTask> {

        private final int priority = 0;
        protected final RobotInterface robot;
        protected final ParcelInterface parcel;

        public ParcelTask(RobotInterface robot, ParcelInterface parcel) {
            this.robot = robot;
            this.parcel = parcel;
        }

        @Override
        protected ParcelInterface call() throws Exception {
            throw new NotImplementedException();
        }

        @Override
        public void run() {
            throw new NotImplementedException();
        }

        protected RobotInterface getRobot() {
            return robot;
        }

        public ParcelInterface getParcel() {
            return parcel;
        }

        @Override
        public int compareTo(ParcelTask o) {
            return compareTo(this, o);
        }

        public int getPriority() {
            return priority;
        }
    }

    private class PeekParcelTask extends ParcelTask {

        private final int priority = 1;
        private int USid;
        private int parcelId;
        private RobotInterface robot;
        private ParcelInterface parcel;

        public PeekParcelTask(int USid, int parcelId, RobotInterface robot) {
            super(robot, null);
            this.USid = USid;
            this.parcelId = parcelId;
            this.robot = robot;
        }

        @Override
        protected ParcelInterface call() throws Exception {
            try {
                robot.peek(USid, parcelId);
            } catch (Exception e) {
                logger.warning("Robot probably break");
                brokenParcelRegister.add(parcel);
                throw e;
            }
            if (storageUnitRegister.get(USid) == null) {
                logger.warning(String.format("No US with id %d", USid));
            } else {
                parcel = storageUnitRegister.get(USid).stream().filter(p -> p.getParcelID() == parcelId).findAny().orElse(null);
                if (parcel == null) {
                    logger.warning("Parcel is not in US. It maybe in broken parcel register");
                    parcel = brokenParcelRegister.stream().filter(p -> p.getParcelID() == parcelId).findAny().orElse(null);
                    if (parcel == null) {
                        logger.severe("No parcel in system with id: " + parcelId);
                    }
                }
            }
            robotRegister.add(robot);
            return parcel;
        }
    }

    private class PushParcelTask extends ParcelTask {

        private final int priority = 2;
        private ParcelInterface parcel;
        private int USid;
        private RobotInterface robot;

        public PushParcelTask(ParcelInterface parcel, int USid, RobotInterface robot) {
            super(robot, parcel);
            this.parcel = parcel;
            this.USid = USid;
            this.robot = robot;
        }

        @Override
        public void run() {
            if (storageUnitRegister.get(USid) == null) {
                logger.warning(String.format("No US with id %d", USid));
                return;
            }
            try {
                robot.push(parcel, USid);
            } catch (Exception e) {
                logger.warning("Robot probably break");
                brokenParcelRegister.add(parcel);
                throw e;
            }
            if (storageUnitRegister.get(USid) == null) {
                logger.warning(String.format("No US with id %d", USid));
            } else {
                storageUnitRegister.get(USid).add(parcel);
            }
            robotRegister.add(robot);
        }
    }
}

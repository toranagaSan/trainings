package macwac.zadanie1;

/**
 * @author Pawel Tarsa (SG0953429)
 * @since 4/4/2017.
 */

public interface RobotInterface {
    /**
     * Zlecenie umieszczenia paczki w US o podanym numerze.
     *
     * @param parcel
     *            paczka do umieszczenia w US
     * @param USid
     *            identyfikator US
     */
    void push(ParcelInterface parcel, int USid);

    /**
     * Zlecenie pobrania z US o numerze USid paczki o numerze parcelID.
     *
     * @param USid
     *            numer US
     * @param parcelID
     *            numer paczki
     * @return Metoda zwraca paczke o wskazanym parcelID o ile w danym US jest
     *         dostepna. Jesli paczki o wskazanym parcelID nie ma, to metoda
     *         zwraca null.
     * @throws Exception
     *             wystapienie wyjatku oznacza uszkodzenie robota w drodze po
     *             paczke. Robot nie nadaje sie juz do uzycia.
     */
    ParcelInterface peek(int USid, int parcelID) throws Exception;
}

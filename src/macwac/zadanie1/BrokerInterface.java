package macwac.zadanie1;

/**
 * @author Pawel Tarsa (SG0953429)
 * @since 4/4/2017.
 */
public interface BrokerInterface {

    /**
     * Metoda ustala liczbe urzadzen skladujacych. Wywolywana jest jednokrotnie
     * przed wprowadzeniem do systemu pierwszego robota i przed pojawieniem sie
     * pierwszego uzytkownika z paczka. Jednostki US numerowane sa od 0 do
     * pomniejszonej o 1 przekazanej tu ich liczby.
     *
     * @param USs
     *            liczba jednostek skladujacych.
     */
    void setNumberOfStorageUnits(int USs);

    /**
     * Metoda udostepnia posrednikowi nowego robota.
     *
     * @param robot
     *            nowy udostepniony robot
     */
    void addRobot(RobotInterface robot);

    /**
     * Zlecenie umieszczenia paczki w US o podanym numerze.
     *
     * @param parcel
     *            paczka do umieszczenia w US
     * @param USid
     *            identyfikator US
     */
    void push(ParcelInterface parcel, int USid);

    /**
     * Zlecenie pobrania z US o numerze USid paczki o numerze parcelID.
     *
     * @param USid
     *            numer US
     * @param parcelID
     *            numer paczki
     * @return Metoda zwraca paczke o wskazanym parcelID o ile w danym US jest
     *         dostepna. Jesli paczki o wskazanym parcelID nie ma, to metoda
     *         zwraca null.
     */
    ParcelInterface peek(int USid, int parcelID);
}
package macwac.zadanie1;

/**
 * @author Pawel Tarsa (SG0953429)
 * @since 4/4/2017.
 */
public class ParcelImpl implements ParcelInterface {

    public static int parcelGuid;
    private final int parcelId;

    static {
        parcelGuid = 0;
    }

    public ParcelImpl(int parcelId) {
        this.parcelId = parcelId;
    }

    public static void main(String[] args) {

    }

    @Override
    public int getParcelID() {
        return parcelId;
    }
}

/**
 * Interfejs paczki.
 *
 */
public interface ParcelInterface {
	/**
	 * Metoda zwraca identyfikator paczki. Identyfikator jest unikalny w calym
	 * systemie.
	 * 
	 * @return uniklany identyfikator paczki
	 */
	public int getParcelID();
}

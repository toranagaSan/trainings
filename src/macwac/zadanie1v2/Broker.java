import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;

public class Broker implements BrokerInterface {

   private final RobotsHolder robots;
   private final AtomicInteger numberOfParcelsToPush;
   private StorageDevice[] storageDevices;
   private static final ExecutorService executorService;

   static {
      executorService = Executors.newCachedThreadPool();
   }

   public Broker() {
      this.robots = new RobotsHolder();
      numberOfParcelsToPush = new AtomicInteger(0);
   }

   @Override
   public void setNumberOfStorageUnits(final int USs) {
      storageDevices = new StorageDevice[USs];
      IntStream.range(0, USs).forEach(id -> storageDevices[id] = new StorageDevice(id));

      executorService.execute(() -> checksIfThereAreParcelsToPush());
   }

   private void checksIfThereAreParcelsToPush() {
      while (true) {
         for (StorageDevice storageDevice : storageDevices) {
            if (storageDevice.hasParcelsToPush() && storageDevice.isNotLocked()) {
               executorService.execute(() -> pushParcel(storageDevice));
            }
         }
      }
   }

   private void pushParcel(final StorageDevice storageDevice) {
      storageDevice.lock();
      if (storageDevice.hasParcelsToPush()) {
         final ParcelInterface parcel = storageDevice.getFirstParcelFromBufferToPush();
         robots.push(parcel, storageDevice.getId());
         numberOfParcelsToPush.decrementAndGet();
      }
      storageDevice.unlock();
   }

   @Override
   public void addRobot(final RobotInterface robot) {
      this.robots.addRobot(robot);
   }

   @Override
   public synchronized void push(final ParcelInterface parcel, final int USid) {
      storageDevices[USid].addLast(parcel);
   }

   @Override
   public ParcelInterface peek(final int USid, final int parcelID) {
      final StorageDevice storageDevice = storageDevices[USid];
      try {
         storageDevice.lock();

         final ParcelInterface parcelFromBuffer = storageDevice.getParcelFromBuffer(parcelID);
         if (parcelFromBuffer != null) {
            return parcelFromBuffer;
         }

         return robots.peek(USid, parcelID);
      } catch (InterruptedException e) {
         throw new RuntimeException(e);
      } finally {
         storageDevice.unlock();
      }
   }

   public static class RobotsHolder implements RobotInterface {
      private final BlockingQueue<RobotInterface> availableRobots;

      public RobotsHolder() {
         this.availableRobots = new LinkedBlockingQueue<>();
      }

      public void addRobot(final RobotInterface robot) {
         this.availableRobots.add(robot);
      }

      @Override
      public void push(final ParcelInterface parcel, final int USid) {
         try {
            final RobotInterface robot = availableRobots.take();
            robot.push(parcel, USid);
            availableRobots.add(robot);
         } catch (InterruptedException e) {
            new RuntimeException(e);
         }
      }

      @Override
      public ParcelInterface peek(final int USid, final int parcelID) throws InterruptedException {
         final RobotInterface robot = availableRobots.take();
         try {
            final ParcelInterface parcel = robot.peek(USid, parcelID);
            availableRobots.put(robot);
            return parcel;
         } catch (Exception e) {
            return peek(USid, parcelID); // retry with another robot
         }
      }
   }

   public static class StorageDevice {
      private final int id;
      private final ReentrantLock available;
      private final HashSet<Integer> parcelsInBuffer;
      private final LinkedList<ParcelInterface> inputParcelBuffer;

      public StorageDevice(final int id) {
         this.id = id;
         this.available = new ReentrantLock();
         this.inputParcelBuffer = new LinkedList<>();
         parcelsInBuffer = new HashSet<>();
      }

      public synchronized void addLast(final ParcelInterface parcel) {
         parcelsInBuffer.add(parcel.getParcelID());
         inputParcelBuffer.addLast(parcel);
      }

      public void lock() {
         available.lock();
      }

      public void unlock() {
         available.unlock();
      }

      public boolean hasParcelsToPush() {
         return !inputParcelBuffer.isEmpty();
      }

      public ParcelInterface getFirstParcelFromBufferToPush() {
         final ParcelInterface parcel = inputParcelBuffer.removeFirst();
         parcelsInBuffer.remove(parcel.getParcelID());
         return parcel;
      }

      public ParcelInterface getParcelFromBuffer(final int parcelId) {
         if (!parcelsInBuffer.contains(parcelId)) {
            return null;
         }
         final Iterator<ParcelInterface> iterator = inputParcelBuffer.iterator();
         while (iterator.hasNext()) {
            ParcelInterface parcel = iterator.next();
            if (parcel.getParcelID() == parcelId) {
               iterator.remove();
               return parcel;
            }
         }
         return null;
      }

      public int getId() {
         return id;
      }

      public boolean isNotLocked() {
         return !available.isLocked();
      }
   }
}

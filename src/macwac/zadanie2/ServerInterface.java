/**
 * Interfejs serwera przetwarzajacego zadania.
 */
public interface ServerInterface {
	/**
	 * Metoda przetwarza przekazane zadanie-obiekt. Metoda nie jest przeznaczona
	 * do wywolywania wspolbieznego. Jeden obiekt moze przetwarzac tylko jedno
	 * zadanie w danym czasie.
	 * 
	 * @param task
	 *            zadanie do przetworzenia
	 */
	public void process(TaskInterface task);
}

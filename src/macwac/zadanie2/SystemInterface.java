import java.util.concurrent.BlockingQueue;

/**
 * Interfejs systemu budowy i symulacji przeplywu zadan przez rozbudowany system
 * kolejkowy. Wszystkie metody przeznaczone sa do wspolbieznego wywolywania
 * przez wiele watkow.
 * 
 * @author oramus
 *
 */
public interface SystemInterface {
	/**
	 * Typ wyliczeniowy okreslajacy typ PU.
	 * <ul>
	 * <li>TERMINAL - PU, ktore po przetworzeniu zadania za pomoca serwera nie
	 * umieszcza go w kolejce kolejnego PU
	 * <li>NORMAL - PU, ktore przetwarzaja zadania i przekazuja je dalej w
	 * systemie.
	 * </ul>
	 *
	 */
	public enum NodeType {
		NORMAL, TERMINAL,
	}

	/**
	 * Metoda tworzy PU wskazanego typu i z przekazanymi jako parametry serwerem
	 * i kolejka.
	 * 
	 * @param type
	 *            typ PU
	 * @param queue
	 *            kolejka do przechowywania zadan
	 * @param server
	 *            serwer obslugujacy zadania w tym PU
	 * @return Unikalny w skali calego systemu numer identyfikujacy PU.
	 */
	public int createNode(NodeType type, BlockingQueue<TaskInterface> queue, ServerInterface server);

	/**
	 * Metoda laczy "wyjscie" zrodlowego PU z "wejsciem" kolejki docelowego PU.
	 * W przypadku podlaczenia do jednego zrodlowego PU wielu odbiorcow, zadania
	 * rozdzielane sa wg. algorytmu karuzelowego (Round robin).
	 * 
	 * @param srcID
	 *            identyfikator zrodlowego PU (zrodla zadan)
	 * @param dstID
	 *            identyfikator docelowego PU (odbiorcy zadan)
	 */
	public void connect(int srcID, int dstID);

	/**
	 * Metoda rozpina "wyjscie" zrodlowego PU z kolejka docelowego PU.
	 * 
	 * @param srcID
	 *            identyfikator zrodlowego PU (zrodla zadan)
	 * @param dstID
	 *            identyfikator docelowego PU (odbiorcy zadan)
	 */
	public void disconnect(int srcID, int dstID);

	/**
	 * Metoda wprowadza zadanie do kolejki PU o podanym numerze
	 * identyfikacyjnym. Zadania moga byc wprowadzone w dowolnym momencie do
	 * dowolnego z istniejacych PU, ktory posiada podlaczonego odbiorce lub jest
	 * typu TERMINAL.
	 * 
	 * @param task
	 *            zadanie do wprowadzenia do kolejki
	 * @param nodeID
	 *            numer PU, do ktorego zadanie ma trafic
	 */
	public void insertTask(TaskInterface task, int nodeID);
}

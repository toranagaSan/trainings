import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

class System implements SystemInterface {

   private Map<Integer, ProcessingUnit> processingUnits;
   private AtomicInteger processingUnitIdGenerator = new AtomicInteger();

   public System() {
      processingUnits = new ConcurrentHashMap<>();
   }

   @Override
   public int createNode(final NodeType type,
                         final BlockingQueue<TaskInterface> queue,
                         final ServerInterface server) {
      final int puId = processingUnitIdGenerator.incrementAndGet();
      final ProcessingUnit processingUnit = new ProcessingUnit(type, queue, server, this, puId);
      processingUnits.put(puId, processingUnit);
      processingUnit.start();
      return puId;
   }

   @Override
   public void connect(final int srcID, final int dstID) {
      processingUnits.get(srcID).addOutputListeningPU(dstID);
   }

   @Override
   public void disconnect(final int srcID, final int dstID) {
      processingUnits.get(srcID).removeOutputListeningPU(dstID);
   }

   @Override
   public void insertTask(final TaskInterface task, final int nodeID) {
      processingUnits.get(nodeID).addTaskToQueue(task);
   }

   public void stopProcessing() {
      for (ProcessingUnit pu : processingUnits.values()) {
         pu.stopProcessingUnit();
      }
   }



   class ProcessingUnit extends Thread {
      private final SystemInterface system;
      private final boolean normalNode;
      private final BlockingQueue<TaskInterface> inputQueue;
      private final ServerInterface server;
      private int id;
      private final List<Integer> outputListeningProcessionUnits;
      private int lastOutputListeningPUIndex;
      private volatile boolean isAlive;

      ProcessingUnit(final NodeType type,
                     final BlockingQueue<TaskInterface> inputQueue,
                     final ServerInterface server, SystemInterface system,
                     final int id) {
//      java.lang.System.out.println(String.format("create pu %d", id));
         this.system = system;
         this.normalNode = NodeType.NORMAL == type;
         this.inputQueue = inputQueue;
         this.server = server;
         this.id = id;
         this.outputListeningProcessionUnits = new ArrayList<>();
         this.lastOutputListeningPUIndex = -1;
      }

      synchronized void addOutputListeningPU(final int processingUnitId) {
         outputListeningProcessionUnits.add(processingUnitId);
      }
      synchronized void removeOutputListeningPU(final int processingUnitId) {
         outputListeningProcessionUnits.remove(processingUnitId);
      }

      void addTaskToQueue(final TaskInterface task) {
//      java.lang.System.out.println(String.format("add task: pu %d task: %s", id, task));
         this.inputQueue.add(task);
      }

      @Override
      public void run() {
         try {
            isAlive = true;
            startProcessingTasks();
         } catch (InterruptedException e) {
            e.printStackTrace();
         }
      }

      private void startProcessingTasks() throws InterruptedException {
         while(isAlive) {
            final TaskInterface task = inputQueue.take();
            server.process(task);

            if (normalNode) {
               final Integer nextProcessingUnit = getNextProcessingUnit();
               if (nextProcessingUnit != null) {
                  system.insertTask(task, nextProcessingUnit);
               }
            }
         }
      }

      private synchronized Integer getNextProcessingUnit() {
         if (outputListeningProcessionUnits.isEmpty()) {
            return null;
         }
         lastOutputListeningPUIndex = ++lastOutputListeningPUIndex % outputListeningProcessionUnits.size();
         return outputListeningProcessionUnits.get(lastOutputListeningPUIndex);
      }

      public void stopProcessingUnit() {
         isAlive = false;
      }
   }
}
public class FixedTimeTask implements TaskInterface {

   private final long taskLengthMills;

   public FixedTimeTask(final long taskLengthMills) {
      this.taskLengthMills = taskLengthMills;
   }

   public void execute() {
      try {
         Thread.sleep(taskLengthMills);
      } catch (InterruptedException e) {
         e.printStackTrace();
      }
   }

}

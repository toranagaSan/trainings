import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;
import java.util.concurrent.LinkedBlockingDeque;

public class Zadanie2Test {
   private System system;
   private Random random;

   @Before
   public void setUp() {
      system = new System();
      random = new Random();
   }

   @Test
   public void test() throws InterruptedException {
      final Server server1 = new Server();
      final Server server2 = new Server();
      final Server server3 = new Server();
      final Server server4 = new Server();
      final int node1 = createNode(SystemInterface.NodeType.NORMAL, server1);
      final int node2 = createNode(SystemInterface.NodeType.NORMAL, server2);
      final int node3 = createNode(SystemInterface.NodeType.NORMAL, server3);
      final int node4 = createNode(SystemInterface.NodeType.TERMINAL, server4);

      system.connect(node1, node2);
      system.connect(node1, node3);
      system.connect(node2, node4);
      system.connect(node3, node4);

      insertTask(node1, 100, 200);
      insertTask(node1, 100, 200);
      insertTask(node1, 100, 200);
      insertTask(node1, 100, 200);

      Thread.sleep(10000);
      system.stopProcessing();

      Assert.assertEquals(4, server1.getNumberOfTaskProcessed());
      Assert.assertEquals(2, server2.getNumberOfTaskProcessed());
      Assert.assertEquals(2, server3.getNumberOfTaskProcessed());
      Assert.assertEquals(4, server4.getNumberOfTaskProcessed());
   }




   private void insertTask(final int nodeId, final int taskLengthMin) {
      insertTask(nodeId, taskLengthMin, taskLengthMin);
   }
   private void insertTask(final int nodeId, final int taskLengthMin, final int taskLengthMax) {
      final long taskLength = taskLengthMin >= taskLengthMax
            ? taskLengthMin : taskLengthMin + random.nextInt(taskLengthMax - taskLengthMin);
      system.insertTask(new FixedTimeTask(taskLength), nodeId);
   }
   private int createNode(final SystemInterface.NodeType nodeType, final Server server) {
      return system.createNode(nodeType, new LinkedBlockingDeque<>(), server);
   }

}

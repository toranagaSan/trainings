public class Server implements ServerInterface {
   private volatile boolean busy;
   private long numberOfTaskProcessed = 0;

   @Override
   public void process(final TaskInterface task) {
      if (busy)
         throw new IllegalStateException("Server is busy: " + this);
      busy = true;
      ((FixedTimeTask) task).execute();
      ++numberOfTaskProcessed;
      busy = false;
   }

   public long getNumberOfTaskProcessed() {
      return numberOfTaskProcessed;
   }
}
